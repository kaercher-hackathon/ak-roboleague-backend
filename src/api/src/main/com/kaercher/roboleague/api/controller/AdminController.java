package com.kaercher.roboleague.api.controller;


import com.kaercher.roboleague.api.annotations.Route;
import com.kaercher.roboleague.api.dto.ConnectionStatus;
import com.kaercher.roboleague.api.interfaces.IHttpResult;
import spark.Request;

import static com.kaercher.roboleague.api.annotations.Route.HttpMethod.GET;
import static com.kaercher.roboleague.api.annotations.Route.HttpMethod.POST;
import static com.kaercher.roboleague.api.httpresult.HttpResults.Ok;

public class AdminController extends ApiController {

    @Route(path = "/admin/connection", method = GET)
    public IHttpResult getConnectionStatus(Request request) {
        com.kaercher.roboleague.middleware.model.GameStatus status = this.middleware.getGameController().getGameStatus();
        ConnectionStatus connectionStatus = new ConnectionStatus();
        connectionStatus.homeTeam = status.getHomePushCommandReceiver().isPresent();
        connectionStatus.guestTeam = status.getGuestPushCommandReceiver().isPresent();
        status.getHomeRobot().ifPresent(robot -> connectionStatus.homeUid = robot.getUid());
        status.getGuestRobot().ifPresent(robot -> connectionStatus.guestUid = robot.getUid());
        return Ok(connectionStatus);
    }

    @Route(path = "/admin/goal/home", method = POST)
    public IHttpResult setHomeGoal(Request request) {
        this.middleware.addHomeGoal();
        return Ok();
    }

    @Route(path = "/admin/goal/guest", method = POST)
    public IHttpResult setGuestGoal(Request request) {
        this.middleware.addGuestGoal();
        return Ok();
    }
}
