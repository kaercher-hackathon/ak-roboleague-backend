package com.kaercher.roboleague.api.dto;


public class GameItem {

    private String identifier;

    private long duration;

    private long remaining;

    public GameItem(com.kaercher.roboleague.middleware.model.GameItem gameItem) {
        this.identifier = gameItem.getType().name();
        remaining = gameItem.getRemaining();
        duration = gameItem.getDuration();
    }
}
