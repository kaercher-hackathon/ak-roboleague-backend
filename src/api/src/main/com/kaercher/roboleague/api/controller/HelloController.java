package com.kaercher.roboleague.api.controller;

import com.kaercher.roboleague.api.annotations.Route;
import com.kaercher.roboleague.api.interfaces.IHttpResult;
import spark.Request;
import spark.Response;

import static com.kaercher.roboleague.api.annotations.Route.HttpMethod.GET;
import static com.kaercher.roboleague.api.annotations.Route.HttpMethod.OPTIONS;
import static com.kaercher.roboleague.api.httpresult.HttpResults.NotFound;
import static com.kaercher.roboleague.api.httpresult.HttpResults.Ok;
import static spark.Spark.get;

public class HelloController extends ApiController {

    @Route(path = "/hello", method = GET)
    public IHttpResult getHello(Request request) {
        return Ok("Hello");
    }

    @Route(path = "/hello/:name", method = GET)
    public IHttpResult getName(Request request) {
        return Ok("Hello " + request.params(":name"));
    }

    @Route(path = "/bye", method = GET)
    public IHttpResult getNull(Request request) {
        return NotFound();
    }

}
