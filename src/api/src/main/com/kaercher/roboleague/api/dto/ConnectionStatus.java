package com.kaercher.roboleague.api.dto;

public class ConnectionStatus {

    public boolean homeTeam;
    public boolean guestTeam;

    public String homeUid;
    public String guestUid;
}
