package com.kaercher.roboleague.api.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaercher.roboleague.api.annotations.Route;
import com.kaercher.roboleague.api.dto.GameStatus;
import com.kaercher.roboleague.api.interfaces.IHttpResult;
import spark.Request;

import static com.kaercher.roboleague.api.annotations.Route.HttpMethod.GET;
import static com.kaercher.roboleague.api.annotations.Route.HttpMethod.POST;
import static com.kaercher.roboleague.api.httpresult.HttpResults.BadRequest;
import static com.kaercher.roboleague.api.httpresult.HttpResults.Forbidden;
import static com.kaercher.roboleague.api.httpresult.HttpResults.Ok;

public class GameStatusController extends ApiController {

    @Route(path = "/game/status", method = GET)
    public IHttpResult getStatus(Request request) {
        GameStatus status = new GameStatus(this.middleware.getGameController().getGameStatus());
        return Ok(status);
    }

    @Route(path = "/game/team/home/name", method = POST)
    public IHttpResult setHomeTeamName(Request request) {

        JsonObject body = new Gson().fromJson(request.body(), JsonObject.class);
        if (body.has("name")) {
            String name = body.get("name").getAsString();
            this.middleware.getGameController().setHomeTeamName(name);
            return Ok();
        }
        return BadRequest("name property not present");
    }

    @Route(path = "/game/team/guest/name", method = POST)
    public IHttpResult setGuestTeamName(Request request) {
        JsonObject body = new Gson().fromJson(request.body(), JsonObject.class);
        if (body.has("name")) {
            String name = body.get("name").getAsString();
            this.middleware.getGameController().setGuestTeamName(name);
            return Ok();
        }
        return BadRequest("name property not present");
    }

    @Route(path = "/game/new", method = POST)
    public IHttpResult newGame(Request request) {
        if (this.middleware.getGameController().newGame())
            return Ok();
        else
            return Forbidden();
    }

    @Route(path = "/game/start", method = POST)
    public IHttpResult startGame(Request request) {
        if (this.middleware.getGameController().startGame())
            return Ok();
        else
            return Forbidden();
    }


    @Route(path = "/game/pause", method = POST)
    public IHttpResult pauseGame(Request request) {
        if (this.middleware.getGameController().pauseGame())
            return Ok();
        else
            return Forbidden();
    }


    @Route(path = "/game/resume", method = POST)
    public IHttpResult resumeGame(Request request) {
        if (this.middleware.getGameController().resumeGame())
            return Ok();
        else
            return Forbidden();
    }
}
