package com.kaercher.roboleague.api.controller;


import com.kaercher.roboleague.api.annotations.Route;
import com.kaercher.roboleague.api.dto.Robot;
import com.kaercher.roboleague.api.interfaces.IHttpResult;
import spark.Request;

import java.util.stream.Collectors;

import static com.kaercher.roboleague.api.httpresult.HttpResults.Ok;

public class RobotStatusController extends ApiController {

    @Route(path = "/robot/status", method = Route.HttpMethod.GET)
    public IHttpResult getRobotStatus(Request request) {
        return Ok(this.middleware.getRobotController().getRobots().stream().map(Robot::new).collect(Collectors.toList()));
    }
}
