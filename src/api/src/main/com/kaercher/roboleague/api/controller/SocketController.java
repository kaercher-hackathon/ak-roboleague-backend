package com.kaercher.roboleague.api.controller;

import com.kaercher.roboleague.api.annotations.Route;
import com.kaercher.roboleague.api.interfaces.IHttpResult;
import spark.Request;

import static com.kaercher.roboleague.api.annotations.Route.HttpMethod.PUT;
import static com.kaercher.roboleague.api.httpresult.HttpResults.BadRequest;
import static com.kaercher.roboleague.api.httpresult.HttpResults.Ok;

public class SocketController extends ApiController {

    @Route(path = "/socket/register/:uid", method = PUT)
    public IHttpResult registerSocket(Request request) {
        String sessionKey = request.queryMap("key").value();
        String robotUid = request.params(":uid");
        if (this.middleware.clientSocketSubmit(sessionKey, robotUid))
            return Ok(sessionKey);
        else
            return BadRequest("robot or session not found");
    }
}
