package com.kaercher.roboleague.api.util;


import com.google.gson.Gson;
import spark.ResponseTransformer;

public class JsonUtil {

    public static String toJson(Object obj) {
        return new Gson().toJson(obj);
    }

    public static ResponseTransformer json() {
        return JsonUtil::toJson;
    }
}
