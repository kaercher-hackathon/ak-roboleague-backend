package com.kaercher.roboleague.api.httpresult;

import com.kaercher.roboleague.api.interfaces.IHttpResult;

public final class HttpResults {

    public static IHttpResult Ok(Object obj) {
        OkHttpResult result = new OkHttpResult();
        result.setBody(obj);
        return result;
    }

    public static IHttpResult Ok() {
        return new OkHttpResult();
    }

    public static IHttpResult NotFound(Object obj) {
        NotFoundHttpResult result = new NotFoundHttpResult();
        result.setBody(obj);
        return result;
    }

    public static IHttpResult NotFound() {
        return new NotFoundHttpResult();
    }

    public static IHttpResult BadRequest(Object obj) {
        BadRequestHttpResult result = new BadRequestHttpResult();
        result.setBody(obj);
        return result;
    }

    public static IHttpResult BadRequest() {
        return new BadRequestHttpResult();
    }

    public static IHttpResult Forbidden(Object obj) {
        ForbiddenHttpResult result = new ForbiddenHttpResult();
        result.setBody(obj);
        return result;
    }

    public static IHttpResult Forbidden() {
        return new ForbiddenHttpResult();
    }


}
