package com.kaercher.roboleague.api.annotations;

import java.lang.annotation.*;

@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Route {

    enum HttpMethod {
        GET,
        POST,
        PUT,
        DELETE,
        OPTIONS
    }

    String path();

    HttpMethod method() default HttpMethod.GET;
}


