package com.kaercher.roboleague.api.httpresult;

import com.kaercher.roboleague.api.interfaces.IHttpResult;

/**
 * Created by Jan Koschke on 30.08.2018.
 */
public class NotFoundHttpResult implements IHttpResult {

    @Override
    public void setBody(Object obj) {

    }

    @Override
    public Object getBody() {
        return null;
    }
}
