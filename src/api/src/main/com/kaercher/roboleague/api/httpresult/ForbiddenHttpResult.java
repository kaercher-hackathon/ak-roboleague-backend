package com.kaercher.roboleague.api.httpresult;


import com.kaercher.roboleague.api.interfaces.IHttpResult;

public class ForbiddenHttpResult implements IHttpResult {

    @Override
    public void setBody(Object obj) {
    }

    @Override
    public Object getBody() {
        return null;
    }
}
