package com.kaercher.roboleague.api.controller;


import com.kaercher.roboleague.api.annotations.Route;
import com.kaercher.roboleague.api.interfaces.IHttpResult;
import spark.Request;

import static com.kaercher.roboleague.api.annotations.Route.HttpMethod.GET;
import static com.kaercher.roboleague.api.httpresult.HttpResults.Ok;

public class DashboardController extends ApiController {

    @Route(path = "/ranking", method = GET)
    public IHttpResult getRanking(Request request) {
        return Ok(this.middleware.getRanking());
    }

    @Route(path = "/stats", method = GET)
    public IHttpResult getStatistic(Request request) {
        return Ok(this.middleware.getStatistics());
    }
}
