package com.kaercher.roboleague.api.interfaces;

public interface IHttpResult {

    void setBody(Object obj);

    Object getBody();
}
