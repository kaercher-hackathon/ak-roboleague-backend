package com.kaercher.roboleague.api.httpresult;

import com.kaercher.roboleague.api.interfaces.IHttpResult;

public class BadRequestHttpResult implements IHttpResult {
    private Object body;

    @Override
    public void setBody(Object obj) {
        this.body = obj;
    }

    @Override
    public Object getBody() {
        return body;
    }


}
