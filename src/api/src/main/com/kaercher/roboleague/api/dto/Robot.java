package com.kaercher.roboleague.api.dto;


public class Robot {

    private String uid;

    private String ip;

    private String team;

    private boolean isInNetwork;

    private int battery;

    private String mode;

    private String charging;

    public Robot(com.kaercher.roboleague.middleware.model.Robot robot) {
        this.uid = robot.getUid();
        this.ip = robot.getIp();
        this.team = robot.getTeam().name();
        this.isInNetwork = robot.getInNetwork();
        this.battery = robot.getBattery();
        this.mode = robot.getMode();
        this.charging = robot.getCharging();
    }

}
