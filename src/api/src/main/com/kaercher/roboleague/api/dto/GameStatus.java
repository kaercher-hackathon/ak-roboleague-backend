package com.kaercher.roboleague.api.dto;

import java.util.List;
import java.util.stream.Collectors;

public class GameStatus {

    private long remainingTime;
    private int countdownTime;
    private String status;

    private String homeName;
    private int homeGoals;
    private int homePoints;
    private List<GameItem> homeItems;
    private List<GameItem> activeHomeItems;

    private String guestName;
    private int guestGoals;
    private int guestPoints;
    private List<GameItem> guestItems;
    private List<GameItem> activeGuestItems;

    public GameStatus(com.kaercher.roboleague.middleware.model.GameStatus status) {
        this.remainingTime = status.getRemainingTime();
        this.countdownTime = status.getCountdownTime();
        this.status = status.getStatus().toString();
        this.homeName = status.getHomeName();
        this.homeGoals = status.getHomeGoals();
        this.homePoints = status.getHomePoints();

        this.guestGoals = status.getGuestGoals();
        this.guestName = status.getGuestName();
        this.guestPoints = status.getGuestPoints();

        this.homeItems = status.getHomeItemsSnapshot().stream().map(GameItem::new).collect(Collectors.toList());
        this.activeHomeItems = status.getActiveHomeItemsSnapshot().stream().map(GameItem::new).collect(Collectors.toList());

        this.guestItems = status.getGuestItemsSnapshot().stream().map(GameItem::new).collect(Collectors.toList());
        this.activeGuestItems = status.getActiveGuestItemsSnapshot().stream().map(GameItem::new).collect(Collectors.toList());
    }
}
