package com.kaercher.roboleague.api.controller;


import com.kaercher.roboleague.api.annotations.Route;
import com.kaercher.roboleague.api.interfaces.IHttpResult;
import spark.Request;

import static com.kaercher.roboleague.api.httpresult.HttpResults.Forbidden;
import static com.kaercher.roboleague.api.httpresult.HttpResults.Ok;

public class RobotController extends ApiController {

    @Route(path = "/robot/:uid/forward", method = Route.HttpMethod.GET)
    public IHttpResult Forward(Request request) {
        String uid = request.params(":uid");
        int distance = Integer.parseInt(request.queryParams("distance"));
        int speed = Integer.parseInt(request.queryParams("speed"));
        this.middleware.getRobotController().moveForward(uid, distance, speed);
        return Ok();
    }

    @Route(path = "/robot/:uid/left", method = Route.HttpMethod.GET)
    public IHttpResult Left(Request request) {
        String uid = request.params(":uid");
        int angle = Integer.parseInt(request.queryParams("angle"));
        int speed = Integer.parseInt(request.queryParams("speed"));
        this.middleware.getRobotController().moveLeft(uid, angle, speed);
        return Ok();
    }

    @Route(path = "/robot/:uid/right", method = Route.HttpMethod.GET)
    public IHttpResult Right(Request request) {
        String uid = request.params(":uid");
        int angle = Integer.parseInt(request.queryParams("angle"));
        int speed = Integer.parseInt(request.queryParams("speed"));
        this.middleware.getRobotController().moveRight(uid, angle, speed);
        return Ok();
    }

    @Route(path = "/robot/:uid/item/:slot", method = Route.HttpMethod.GET)
    public IHttpResult activateItem(Request request) {
        String uid = request.params(":uid");
        int slot = Integer.parseInt(request.params(":slot"));
        if (this.middleware.getRobotController().activateItem(uid, slot))
            return Ok();
        else
            return Forbidden();
    }

}
