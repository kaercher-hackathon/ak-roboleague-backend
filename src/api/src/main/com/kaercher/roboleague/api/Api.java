package com.kaercher.roboleague.api;


import com.kaercher.roboleague.api.annotations.Route;
import com.kaercher.roboleague.api.controller.ApiController;
import com.kaercher.roboleague.api.httpresult.BadRequestHttpResult;
import com.kaercher.roboleague.api.httpresult.ForbiddenHttpResult;
import com.kaercher.roboleague.api.httpresult.NotFoundHttpResult;
import com.kaercher.roboleague.api.httpresult.OkHttpResult;
import com.kaercher.roboleague.api.interfaces.IHttpResult;
import com.kaercher.roboleague.middleware.interfaces.IMiddleware;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.reflections.util.ClasspathHelper;
import org.reflections.util.ConfigurationBuilder;
import org.reflections.util.FilterBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import spark.Request;
import spark.Response;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Set;

import static com.kaercher.roboleague.api.util.JsonUtil.json;
import static spark.Spark.*;

public class Api {

    private final Logger LOG = LoggerFactory.getLogger(Api.class);

    private ArrayList<ApiController> ApiController = new ArrayList<>();

    public void start(IMiddleware middleware) {
        System.out.println("Api starting");
        System.out.println("Init controller");

        initExceptionHandler((e -> LOG.error("error", e)));
        before(((request, response) -> {
            //LOG.info(request.pathInfo());
        }));
        after((req, res) -> res.type("application/json"));
        after(((request, response) -> {
            response.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE,OPTIONS");
            response.header("Access-Control-Allow-Origin", "*");
            response.header("Access-Control-Allow-Headers", "Content-Type,Authorization,X-Requested-With,Content-Length,Accept,Origin,");
            response.header("Access-Control-Allow-Credentials", "true");
        }));

        Set<Class<? extends ApiController>> apiController = this.loadApiController();
        this.ApiController = this.initControllerAndRoutes(apiController, middleware);

    }

    private Set<Class<? extends ApiController>> loadApiController() {
        String packageName = Api.class.getPackage().getName();
        Reflections reflections = new Reflections(new ConfigurationBuilder()
                .setUrls(ClasspathHelper.forPackage(packageName))
                .setScanners(new SubTypesScanner(),
                        new TypeAnnotationsScanner())
                .filterInputsBy(new FilterBuilder().includePackage(packageName)));
        return reflections.getSubTypesOf(ApiController.class);

    }

    private ArrayList<ApiController> initControllerAndRoutes(Set<Class<? extends ApiController>> apiControllers, IMiddleware middleware) {
        ArrayList<ApiController> instances = new ArrayList<>();

        for (Class<? extends ApiController> ctrl : apiControllers) {
            try {
                ApiController instance = ctrl.newInstance();

                instances.add(instance);
                instance.setMiddleware(middleware);

                Method[] methods = instance.getClass().getMethods();
                for (Method m : methods) {
                    Annotation[] annotations = m.getDeclaredAnnotations();
                    for (Annotation annotation : annotations) {
                        if (annotation instanceof Route) {
                            Route routeAnnotation = (Route) annotation;
                            this.initRoute(instance, m, routeAnnotation);
                            LOG.info("Added Route " + routeAnnotation.method() + " " + routeAnnotation.path());
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return instances;
    }

    private void initRoute(ApiController instance, Method m, Route routeAnnotation) {

        switch (routeAnnotation.method()) {
            case GET:
                get(routeAnnotation.path(), (request, response) -> invokeApiMethod(instance, m, request, response), json());
                break;
            case POST:
                post(routeAnnotation.path(), (request, response) -> invokeApiMethod(instance, m, request, response), json());
                break;
            case PUT:
                put(routeAnnotation.path(), (request, response) -> invokeApiMethod(instance, m, request, response), json());
                break;
            case DELETE:
                delete(routeAnnotation.path(), (request, response) -> invokeApiMethod(instance, m, request, response), json());
                break;
            case OPTIONS:
                options(routeAnnotation.path(), (request, response) -> invokeApiMethod(instance, m, request, response), json());
                break;
            default:
        }
    }

    private Object invokeApiMethod(ApiController instance, Method m, Request request, Response response) {
        try {
            IHttpResult result = (IHttpResult) m.invoke(instance, request);
            this.mapStatus(response, result);
            return mapBody(result);
        } catch (Exception e) {
            LOG.error("error", e);
            response.status(500);
            return "500 Internal Server Error";
        }
    }

    private void mapStatus(Response response, IHttpResult result) {
        if (result instanceof OkHttpResult) {
            if (result.getBody() == null)
                response.status(204);
            else
                response.status(200);
            return;
        }
        if (result instanceof NotFoundHttpResult) {
            response.status(404);
            return;
        }
        if (result instanceof BadRequestHttpResult) {
            response.status(400);
        }
        if (result instanceof ForbiddenHttpResult) {
            response.status(403);
        }
    }

    private Object mapBody(IHttpResult result) {
        Object _body = result.getBody();
        if (_body == null)
            return "";
        else return _body;
    }

}
