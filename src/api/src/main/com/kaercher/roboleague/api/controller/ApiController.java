package com.kaercher.roboleague.api.controller;


import com.kaercher.roboleague.middleware.interfaces.IMiddleware;

public abstract class ApiController {

    ApiController() {
    }

    IMiddleware middleware;

    public void setMiddleware(IMiddleware middleware) {
        this.middleware = middleware;
    }

}
