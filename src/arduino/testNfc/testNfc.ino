#include <SPI.h>
#include <PN532_SPI.h>
#include <PN532.h>
#include <NfcAdapter.h>
 
PN532_SPI pn532spi(SPI, 10);
NfcAdapter nfc = NfcAdapter(pn532spi);


// Settings
#define Debug false
#define Id    1
const unsigned int MAX_INPUT = 50; // how much serial data we expect before a newline in SerialRX
String Sbuffer = "";
int pin5 = 5 ;
int pin6 = 6;
int pin7 = 7;
int pin8 = 8;
int pinGND = 4 ; //pin 4 used as ground for led
const long interval = 200;

//Variables
unsigned long previousMillis = 0;        // will store last time NFC was checked
unsigned long currentMillis = millis();

void setup(void) {
    Sbuffer.reserve(200); //String with max 200char length
    Serial.begin(57600);
    Serial.setTimeout(10); //for faster response time, default is 1000ms
    delay(200);
    Serial.print("arduino:init:");
    Serial.println(Id);
    if(Debug) Serial.println("INIT NFC TAG READER"); // Header used when using the serial monitor
    pinMode(pin5 , OUTPUT);
    pinMode(pin6 , OUTPUT);
    pinMode(pin7 , OUTPUT);
    pinMode(pin8 , OUTPUT);
    pinMode(pinGND , OUTPUT);
    digitalWrite(pinGND, LOW);
    nfc.begin();
    if(Debug) Serial.println("INITED NFC TAG READER"); // Header used when using the serial monitor
    digitalWrite(pin5, HIGH);
    digitalWrite(pin6, HIGH);
    digitalWrite(pin7, HIGH);
    digitalWrite(pin8, HIGH);
}

void processCommand (const char * data)
{
  if(data[0] == '0'){
    digitalWrite(pin5, LOW);
    digitalWrite(pin6, LOW);
    digitalWrite(pin7, LOW);
    digitalWrite(pin8, LOW);
  }else if (data[0] == '1'){
    digitalWrite(pin5, HIGH);
    digitalWrite(pin6, HIGH);
    digitalWrite(pin7, HIGH);
    digitalWrite(pin8, HIGH);
  } 
  if(Debug) Serial.println(data);
}

void processIncomingByte(const byte inByte)
{
  static char input_line [MAX_INPUT];
  static unsigned int input_pos = 0;
  switch (inByte)
    {
    case '\n':   // end of text
      input_line [input_pos] = 0;  // terminating null byte
      // terminator reached! process input_line here ...
      processCommand (input_line);
      // reset buffer for next time
      input_pos = 0;  
      break;
    case '\r':   // discard carriage return
      break;
    default:
      // keep adding if not full ... allow for terminating null byte
      if (input_pos < (MAX_INPUT - 1))
        input_line [input_pos++] = inByte;
      break;
    }
}

void loop(void) {
  // if serial data available, process it
  while (Serial.available () > 0)   
    processIncomingByte(Serial.read());
  currentMillis = millis();
  
 if (currentMillis - previousMillis >= interval) {
    // save the last time you blinked the LED
    previousMillis = currentMillis;
    if (nfc.tagPresent())
    {
      NfcTag tag = nfc.read();
      if (tag.hasNdefMessage()) // If your tag has a message
      {
        NdefMessage message = tag.getNdefMessage();
        delay(1);
        // If you have more than 1 Message then it wil cycle through them
        int recordCount = message.getRecordCount();
        for (int i = 0; i < recordCount; i++)
        {
          if(Debug) Serial.print("\nNDEF Record ");
          if(Debug) Serial.println(i+1);
          NdefRecord record = message.getRecord(i);
          delay(1);
          int payloadLength = record.getPayloadLength();
          byte payload[payloadLength];
          record.getPayload(payload);
          delay(1);
          String payloadAsString = ""; // Processes the message as a string vs as a HEX value
          for (int c = 0; c < payloadLength; c++) {
            payloadAsString += (char)payload[c];
          }
        
          if(Debug){
              Serial.print("  Information (as String): ");
              Serial.println(payloadAsString);
          }
              Serial.print("arduino:nfc:");
              Serial.print(String(Id));
              Serial.print(":");
              Serial.println(String(payloadAsString));
              delay(100);
        }
      }else{
    }
    }else{
    }
 }
}

 
