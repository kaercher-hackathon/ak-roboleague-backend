# Arduino Software
zum Lesen von NFC Tags und Kommunikation mit einem Raspberry Pi über die Serielle Schnittstelle

Basierend auf https://www.allaboutcircuits.com/projects/read-and-write-on-nfc-tags-with-an-arduino/


## Aublauf Arduino Software:
```javascript
Nach Starten wartet der Arduino auf den Seriellen Befehl:
wartet auf pi:init:X  [X == 0/1]

pi:init:0 ==> Pi startet im Seriell Read Modus
pi:init:1 ==> Pi startet im NFC-Read Modus

Seriell Read Modus:
	wartet auf Seriellen Befehl:
		pi:read:X  [X == 0/1] 
		pi:read:1 ==> wechsle zu NFC-Read Modus
		pi:read:0 ==> bleibe im Seriell Read Modus

		
NFC-Read Modus:
    wartet auf NFC Input
	Sendet alle 500ms einen pi:msgRequest:1
	    wartet anschließend blockierend auf einen Befehl Zeile
	Wenn NFC Karte erkannt:
		=> arduino:nfc:[ARDUINO_ID]:[NFC_KARTEN_INHALT] an Pi schicken
```

## Serielle Befehle:

```
### Arduino ==> Pi

arduino:init:[ARDUINO_ID]  Anmeldung des Arduinos am Pi
arduino:nfc:[ARDUINO_ID]:[NFC_KARTEN_INHALT]
arduino:msgrequest:[ARDUINO_ID]


### Pi ==> Arduino

pi:read:X	[X == 0/1]  Auswahl des Modus des Arduino
pi:led:X	[X == 0/1]   LED AN oder AUS schalten
```	


# Einrichtung:
1. Libarys herunterladen: [NDEF](https://github.com/don/NDEF) und [PN532](https://github.com/Seeed-Studio/PN532)
2. entpacken in \Documents\Arduino\libraries
3. Arduino IDE neustarten
4. evtl Treiber für Arduino Nano installieren
5. in Ndef/Ndef.h die Zeile "#define NDEF_USE_SERIAL" auskommentieren, da die Libary sonst in die Konsole schreibt
	
