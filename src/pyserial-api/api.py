import os
from aiohttp import web
from events import EventDispatcher, SerialDataWriteRequested
import asyncio
import socket
import logging

LOG = logging.getLogger('serialapi.api')

class SerialAPI(EventDispatcher):

    def __init__(self, appctx, **kwargs):
        super(SerialAPI, self).__init__()
        self.loop = kwargs.get('loop') or asyncio.get_event_loop()
        self.appctx = appctx
        self.api_port = kwargs.get('api_port') or 8134
        self.socket_path = kwargs.get('socket_path') or '/var/run/serialapi.sock'
        self._setup_api()

  
    def _setup_api(self):
        self.api_app = web.Application(loop=self.loop) 
        self.api_app.router.add_get('/list', self.list_ttys)
        self.api_app.router.add_get('/{tty}/out', self.output)
        self.api_app.router.add_put('/{tty}/in', self.input)

    def _get_tty(self, tty):
        ttys = [t for t in self.appctx.ttys if t["id"] == tty]
        if len(ttys) == 0:
            return False
        return ttys[0]

    async def list_ttys(self, request):
        return web.json_response([f["id"] for f in self.appctx.ttys])

    async def input(self, request):
        tty = self._get_tty(request.match_info.get('tty'))
        if tty == False:
            return web.Response(text="tty '{}' not found".format(request.match_info.get('tty')), status=404)

        data = await request.read()
        write_ev = SerialDataWriteRequested(tty["id"], data)
        write_ev.trigger()
        return web.Response()

    async def output(self, request):
        tty = self._get_tty(request.match_info.get('tty'))
        if tty == False:
            return web.Response(text="tty '{}' not found".format(request.match_info.get('tty')), status=404)

        while len(tty["buffer"]) == 0:
            await asyncio.sleep(0.1)
        
        return web.Response(text=tty["buffer"].popleft())
            


    def run_api(self):
        # if os.name == "posix":
        #     sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM) #pylint: disable=E1101
        #     if os.path.exists(self.socket_path):
        #         os.remove(self.socket_path)
        #     sock.bind(self.socket_path)
        #     web.run_app(self.api_app, sock=sock)
        # else:
        web.run_app(self.api_app, port=self.api_port)