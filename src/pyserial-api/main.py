#!/usr/bin/python3

import asyncio
import serial_asyncio
import serial
import os
import re
from uart import UARTProtocol
from api import SerialAPI
import logging
import collections
from events import EventDispatcher, SerialDataReceived

LOG = logging.getLogger('serialapi.app')

class PythonSerialApi(EventDispatcher):

    tty_root_path = "/dev" if os.name == "posix" else "D:\Temp\dev"
    tty_re = "ttyUSB[0-9]{1,}$"
    ttys = []
    scan_interval = 3

    def __init__(self):
        super(PythonSerialApi, self).__init__()
        self.loop = asyncio.get_event_loop()
        self.api = SerialAPI(self, loop=self.loop)
        logging.basicConfig(level=logging.DEBUG,
                            format='%(asctime)s %(name)-15s %(levelname)-8s %(message)s',
                            datefmt='%d.%m %H:%M:%S')


    def eval_ttys(self):
        items = [f for f in os.walk(self.tty_root_path)][0][2]
        for item in items:
            if re.match(self.tty_re, item):
                if len([t for t in self.ttys if t["id"] == item]) == 0:
                    self.init_tty(item)

        self.loop.call_at(self.loop.time() + self.scan_interval, self.eval_ttys)

    def init_tty(self, id):
        tty_path = os.path.join(self.tty_root_path, id)
        LOG.info('Initialize new tty -> {}'.format(id))
        new_tty = {
            "id": id,
            "buffer": collections.deque(maxlen=20),
            "coro": serial_asyncio.create_serial_connection(self.loop, lambda: UARTProtocol(id), tty_path, baudrate=57600, bytesize=serial.EIGHTBITS, parity=serial.PARITY_NONE, stopbits=serial.STOPBITS_ONE, xonxoff=True)
        }
        self.ttys.append(new_tty)
        self.loop.create_task(new_tty["coro"])

    def on_serial_data_received(self, ev):
        #LOG.debug('on_serial_data_received')
        ttys = [t for t in self.ttys if t["id"] == ev.tty]
        if len(ttys) > 0:
            ttys[0]["buffer"].append(ev.message)
        else:
            LOG.warning('No tty with id "{}" found'.format(ev.tty))

    def run(self):
        try:
            LOG.debug('Run SerialAPI')
            self.loop.call_at(self.loop.time() + 1, self.eval_ttys)
            self.loop.create_task(self.api.run_api())
            self.loop.run_forever()
        except KeyboardInterrupt:
            LOG.debug("Caught keyboard interrupt. Canceling tasks...")
            self.loop.stop()
            self.loop.close()


if __name__ == "__main__":
    PythonSerialApi().run()