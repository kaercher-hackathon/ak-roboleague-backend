import asyncio
import logging
from events import EventDispatcher, SerialDataReceived, SerialDataWriteRequested

class UARTProtocol(asyncio.Protocol, EventDispatcher):
    buffer = b''
    transport = None
    tty_id = None
    log = None
    log_in = None
    log_out = None
    

    def __init__(self, tty_id):
        super(UARTProtocol, self).__init__()
        self.tty_id = tty_id
        self.log = logging.getLogger('serialapi.uart.{}'.format(self.tty_id))
        self.log_in = logging.getLogger('serialapi.uart.{}.in'.format(self.tty_id))
        self.log_out = logging.getLogger('serialapi.uart.{}.OUT'.format(self.tty_id))
        self.log.info('UART initialized')


    def on_serial_write_requested(self, ev):
        #self.log.debug('on_serial_write_requested')
        if ev.tty == self.tty_id:
            self.send(ev.message)

    def send(self, message):
        if isinstance(message, str):
            message = message.encode()
        self.log_out.debug('Write {}'.format(message))
        self.transport.write(message)
        if self.transport.can_write_eof():
            self.log_out.debug('write eof')
            self.transport.write_eof()

    def connection_made(self, transport):
        self.transport = transport
        self.log.debug('port opened {}'.format(transport))
        transport.serial.rts = False  

    def data_received(self, data):
        #self.log.debug('data_received')
        self.buffer = self.buffer + data
        if b'\n' in self.buffer:
            try:
                b_arr = self.buffer.split(b'\n')
                line = b_arr[0]
                self.buffer = b_arr[1]
                line = str(line, errors='replace')
                self.log_in.debug(line)
                SerialDataReceived(self.tty_id, line).trigger()
            except Exception as exc:
                self.log.error(exc)

    def connection_lost(self, exc):
        self.log.debug('port closed')
        task = asyncio.Task.current_task()
        if task.cancelled():
            task.cancel()

    def pause_writing(self):
        self.log.debug('pause writing')
        self.log.debug(self.transport.get_write_buffer_size())

    def resume_writing(self):
        self.log.debug(self.transport.get_write_buffer_size())
        self.log.debug('resume writing')