import asyncio
import serial
import zope.event

class Event(object):
    source = None
    callback = None
    def __init__(self, callback=None):
        self.callback = callback

    def trigger(self):
        zope.event.notify(self)

class SerialDataReceived(Event):
    tty = None
    message = None
    def __init__(self, tty, message):
        self.tty = tty
        self.message = message

class SerialDataWriteRequested(Event):
    tty = None
    message = None
    def __init__(self, tty, message):
        self.tty = tty
        self.message = message

class EventDispatcher(object):
    
    def __init__(self):
        zope.event.subscribers.append(self.on_test_event)

    def on_serial_data_received(self, event: SerialDataReceived):
        pass

    def on_serial_write_requested(self, event: SerialDataWriteRequested):
        pass

    def on_test_event(self, event):
        if isinstance(event, SerialDataReceived):
            self.on_serial_data_received(event)
        elif isinstance(event, SerialDataWriteRequested):
            self.on_serial_write_requested(event)

