package com.kaercher.roboleague.robotconnector.provider;

import com.kaercher.roboleague.robotconnector.dto.Robot_IdDTO;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class HttpClient {

    private final static Logger LOG = LoggerFactory.getLogger(HttpClient.class);

    public static String get(String ip, String path) throws IOException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet("http://" + ip + ":10009" + path);
        LOG.debug("Request GET: " + request.getURI().toString());
        HttpResponse result = httpClient.execute(request);
        return EntityUtils.toString(result.getEntity(), "UTF-8");
    }
}