package com.kaercher.roboleague.robotconnector.controller;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.kaercher.roboleague.middleware.interfaces.IRobotConnector;
import com.kaercher.roboleague.middleware.model.Robot;
import com.kaercher.roboleague.middleware.model.RobotStatistics;
import com.kaercher.roboleague.robotconnector.provider.HttpClient;
import org.nmap4j.Nmap4j;
import org.nmap4j.core.nmap.NMapExecutionException;
import org.nmap4j.core.nmap.NMapInitializationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RobotConnectorImpl implements IRobotConnector {

    private final static Logger LOG = LoggerFactory.getLogger(RobotConnectorImpl.class);

    @Override
    public void enableDirectMode(Robot r) {
        try {
            HttpClient.get(r.getIp(), "/direct/enable?enabled=1");
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void moveForward(Robot r, int distance, int speed) {
        try {
            distance = distance > 5000 ? 5000 : distance;
            speed = speed > 100000 ? 100000 : speed;
            HttpClient.get(r.getIp(), "/direct/forward?dist=" + distance + "&speed=" + speed);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void moveLeft(Robot r, int angle, int speed) {
        try {
            angle = angle > 2000 ? 2000 : angle;
            speed = speed > 100000 ? 100000 : speed;
            HttpClient.get(r.getIp(), "/direct/turn_left?angle=" + angle + "&ang_speed=" + speed);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void moveRight(Robot r, int angle, int speed) {
        try {
            angle = angle > 2000 ? 2000 : angle;
            speed = speed > 100000 ? 100000 : speed;
            HttpClient.get(r.getIp(), "/direct/turn_right?angle=" + angle + "&ang_speed=" + speed);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void stop(Robot r) {
        try {
            HttpClient.get(r.getIp(), "/direct/back?dist=50&speed=5000");
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void rotate(Robot r, long milliseconds) {
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {

            long millisecondsLeft = milliseconds;

            @Override
            public void run() {
                moveLeft(r, 500, 10000);
                millisecondsLeft = milliseconds - 250;
                if (millisecondsLeft <= 0) {
                    timer.cancel();
                    timer.purge();
                    return;
                }
            }
        }, 0, 250);
    }

    @Override
    public int getBattery(Robot r) {

        int battery_level = 0;

        try {
            String JSON_DATA = HttpClient.get(r.getIp(), "/get/status");
            JsonObject jobj = new Gson().fromJson(JSON_DATA, JsonObject.class);

            battery_level = jobj.get("battery_level").getAsInt();

        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return battery_level;
    }

    @Override
    public String getStatus(Robot r) throws IOException {
        return HttpClient.get(r.getIp(), "/get/status");
    }

    @Override
    public List<String> getRobotIpsInNetwork() {
        ArrayList<String> ipList = new ArrayList<>();
        String pathToExecutable = "/usr";

        if (System.getProperty("os.name").startsWith("Windows"))
            pathToExecutable = "C:\\Program Files (x86)\\Nmap";

        Nmap4j nmap = new Nmap4j(pathToExecutable);
        try {
            nmap.includeHosts("-sP -oG - 192.168.4.0/24");
            nmap.execute();
            String output = nmap.getExecutionResults().getOutput();
            Pattern pattern = Pattern.compile("((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]? (\\(\\)|\\(Robot\\)))");
            Matcher matcher = pattern.matcher(output);

            while (matcher.find()) {
                String match = matcher.group();
                if (match.endsWith(" (Robot)"))
                    ipList.add(match.substring(0, match.length() - " (Robot)".length()));
                else {
                    String ipStub = match.substring(0, match.length() - " ()".length());
                    if (!ipStub.endsWith(".1")) {
                        try {
                            HttpClient.get(ipStub, "/get/robot_id");
                            ipList.add(ipStub);
                        } catch (Exception ex) {
                            LOG.debug(ex.getMessage());
                        }
                    }
                }
            }
        } catch (NMapInitializationException | NMapExecutionException e) {
            LOG.error(e.getMessage(), e);
        }
        return ipList;
    }

    @Override
    public RobotStatistics getRobotStatistics(Robot r) {

        RobotStatistics statistics = null;

        try {
            String JSON_DATA = HttpClient.get(r.getIp(), "/get/statistics");
            JsonObject jobj = new Gson().fromJson(JSON_DATA, JsonObject.class);
            statistics = new RobotStatistics(
                    jobj.get("total_distance_driven").getAsInt(),
                    jobj.get("total_cleaning_time").getAsInt(),
                    jobj.get("total_area_cleaned").getAsInt());

        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        return statistics;
    }


}
