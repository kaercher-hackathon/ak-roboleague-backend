package com.kaercher.roboleague.robotconnector.dto;


public class Robot_IdDTO {


    private String name;
    private String unique_id;
    private String camlas_unique_id;
    private String model;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnique_id() {
        return unique_id;
    }

    public void setUnique_id(String unique_id) {
        this.unique_id = unique_id;
    }

    public String getCamlas_unique_id() {
        return camlas_unique_id;
    }

    public void setCamlas_unique_id(String camlas_unique_id) {
        this.camlas_unique_id = camlas_unique_id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getFirmware() {
        return firmware;
    }

    public void setFirmware(String firmware) {
        this.firmware = firmware;
    }

    private String firmware;

}