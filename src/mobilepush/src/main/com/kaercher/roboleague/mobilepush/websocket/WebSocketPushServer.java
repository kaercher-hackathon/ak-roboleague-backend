package com.kaercher.roboleague.mobilepush.websocket;

import com.kaercher.roboleague.middleware.interfaces.IPushConnector;
import com.kaercher.roboleague.middleware.service.EventBus;
import com.kaercher.roboleague.mobilepush.controller.WebSocketController;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

@WebSocket
public class WebSocketPushServer {

    private final static Logger LOG = LoggerFactory.getLogger(WebSocketPushServer.class);

    private final WebSocketController PUSH_CONTROLLER;

    private List<WebSocketClientConnection> connections = new ArrayList<>();

    public WebSocketPushServer(IPushConnector connector) {
        this.PUSH_CONTROLLER = (WebSocketController) connector;
    }

    public void disconnectAllClients() {
        connections.forEach(WebSocketClientConnection::disconnect);
    }

    void connectionHasClosed(WebSocketClientConnection clientConnection) {
        this.connections.remove(clientConnection);
    }

    @OnWebSocketConnect
    public void connected(Session session) {
        LOG.info(session.getRemoteAddress().toString() + " connected. ");
        WebSocketClientConnection connection = new WebSocketClientConnection(session, this);
        this.connections.add(connection);
        this.PUSH_CONTROLLER.clientConnected(connection);
    }

    @OnWebSocketClose
    public void disconnected(Session session, int statusCode, String reason) {
        Optional<WebSocketClientConnection> connection = this.connections.stream().filter(x -> x.getSession().equals(session)).findFirst();
        connection.ifPresent(conn -> {
            connections.remove(conn);
            EventBus.getInstance().emmit("session_disconnect", conn);
        });
        LOG.info(session.getRemoteAddress().toString() + " disconnected. " + reason);
    }

    @OnWebSocketMessage
    public void message(Session session, String message) {
        System.out.println("WS Message (" + session.getRemoteAddress().toString() + ") : " + message);
    }
}
