package com.kaercher.roboleague.mobilepush.websocket;

import org.eclipse.jetty.websocket.api.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class WebSocketClientConnection {

    private final static Logger LOG = LoggerFactory.getLogger(WebSocketClientConnection.class);

    private final Session SESSION;
    private final WebSocketPushServer SERVER;

    private boolean closeConnection = false;


    private final long CONNECTION_TIMEOUT = 360000;
    private final Timeout CONNECTION_TIMEOUT_WATCHER;

    WebSocketClientConnection(Session session, WebSocketPushServer server) {
        this.SERVER = server;
        this.SESSION = session;
        this.CONNECTION_TIMEOUT_WATCHER = new Timeout(CONNECTION_TIMEOUT);

    }

    public void disconnect() {
        this.closeConnection = true;
        try {
            SESSION.disconnect();
            SESSION.close();
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        } finally {
            SERVER.connectionHasClosed(this);
            CONNECTION_TIMEOUT_WATCHER.end();
        }
    }

    public void writeToClient(String message) {
        try {
            if (SESSION.isOpen())
                SESSION.getRemote().sendString(message);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        this.CONNECTION_TIMEOUT_WATCHER.reset();
    }

    Session getSession() {
        return this.SESSION;
    }


    class Timeout extends Thread {

        private final long timeout;
        private boolean endTimeoutThread = false;
        long counter = 0;

        Timeout(long timeout) {
            this.timeout = timeout;
        }

        @Override
        public void run() {
            while (!endTimeoutThread) {
                try {
                    Thread.sleep(1);
                } catch (InterruptedException ex) {
                    LOG.warn(ex.getMessage(), ex);
                }
                if (counter >= timeout) {
                    disconnect();
                }
                counter++;
            }
        }

        void reset() {
            counter = 0;
        }

        public void end() {
            endTimeoutThread = true;
        }
    }

}
