package com.kaercher.roboleague.mobilepush.model;


import com.kaercher.roboleague.mobilepush.websocket.WebSocketClientConnection;

public class WebSocketCommandReceiver {
    private final WebSocketClientConnection connection;

    public WebSocketCommandReceiver(WebSocketClientConnection connection) {
        this.connection = connection;
    }

    public WebSocketClientConnection getConnection() {
        return connection;
    }
}
