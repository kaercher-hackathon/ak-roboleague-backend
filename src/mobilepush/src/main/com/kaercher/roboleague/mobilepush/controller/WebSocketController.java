package com.kaercher.roboleague.mobilepush.controller;

import com.kaercher.roboleague.middleware.interfaces.IClientConnector;
import com.kaercher.roboleague.middleware.interfaces.IPushConnector;
import com.kaercher.roboleague.middleware.model.PushCommand;
import com.kaercher.roboleague.middleware.model.PushCommandReceiver;
import com.kaercher.roboleague.mobilepush.model.WebSocketCommandReceiver;
import com.kaercher.roboleague.mobilepush.websocket.WebSocketClientConnection;
import com.kaercher.roboleague.mobilepush.websocket.WebSocketPushServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

import static spark.Spark.webSocket;

public class WebSocketController implements IPushConnector {

    private final static Logger LOG = LoggerFactory.getLogger(WebSocketController.class);

    private final WebSocketPushServer SERVER;
    private final IClientConnector CLIENT_CONNECTOR;

    private final HashMap<PushCommandReceiver, WebSocketCommandReceiver> receivers = new HashMap<>();

    public WebSocketController(IClientConnector connector) {
        this.CLIENT_CONNECTOR = connector;
        this.SERVER = new WebSocketPushServer(this);
        webSocket("/", this.SERVER);
    }

    public void clientConnected(WebSocketClientConnection clientConnection) {
        PushCommandReceiver receiver = new PushCommandReceiver(this);
        this.receivers.put(receiver, new WebSocketCommandReceiver(clientConnection));
        this.CLIENT_CONNECTOR.clientConnected(receiver);
    }

    @Override
    public void sendCommand(PushCommandReceiver receiver, PushCommand command) {
        StringBuilder builder = new StringBuilder().append(command.getType().name()).append("?");
        command.getProperties().keySet().forEach(key -> builder.append(key).append("=").append(command.getProperties().get(key)).append("&"));
        String textCmd = builder.toString();
        this.receivers.get(receiver).getConnection().writeToClient(textCmd);
    }

    @Override
    public void disconnectClients() {
        this.SERVER.disconnectAllClients();
    }

    @Override
    public void disconnectClient(PushCommandReceiver receiver) {
        if (this.receivers.containsKey(receiver))
            this.receivers.get(receiver).getConnection().disconnect();
    }

    @Override
    public void start() {

    }

    @Override
    public void close() {

    }
}
