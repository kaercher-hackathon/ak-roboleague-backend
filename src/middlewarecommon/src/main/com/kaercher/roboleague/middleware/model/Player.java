package com.kaercher.roboleague.middleware.model;


public class Player {


    private Robot robot;
    private String name;
    private PushCommandReceiver pushCommandReceiver;
    private int dbID;

    public Player() {
        this.robot = null;
        this.name = "";
        this.pushCommandReceiver = null;
    }

    public Robot getRobot() {
        return robot;
    }

    public void setRobot(Robot robot) {
        this.robot = robot;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PushCommandReceiver getPushCommandReceiver() {
        return pushCommandReceiver;
    }

    public void setPushCommandReceiver(PushCommandReceiver pushCommandReceiver) {
        this.pushCommandReceiver = pushCommandReceiver;
    }

    public int getDbID() {
        return dbID;
    }

    public void setDbID(int id) {
        this.dbID = id;
    }
}
