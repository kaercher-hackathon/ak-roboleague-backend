package com.kaercher.roboleague.middleware.model;

import com.kaercher.roboleague.middleware.interfaces.IPushConnector;

public class PushCommandReceiver {

    private IPushConnector pushConnector;

    public PushCommandReceiver(IPushConnector connector) {
        this.pushConnector = connector;
    }

    public void sendCommand(PushCommand command) {
        this.pushConnector.sendCommand(this, command);
    }
    
}
