package com.kaercher.roboleague.middleware.interfaces;

import com.kaercher.roboleague.middleware.dbo.RobotTeamDbo;
import com.kaercher.roboleague.middleware.model.GameStatus;
import com.kaercher.roboleague.middleware.model.RankingPlayer;
import com.kaercher.roboleague.middleware.model.Robot;
import com.kaercher.roboleague.middleware.model.RobotStatistics;

import java.util.List;
import java.util.Optional;

public interface IDataProvider {


    Optional<String> getTeamForRobotUid(String uid);

    Optional<String> getTeamForTagName(String tag);

    Optional<RobotTeamDbo> getRobotTeamByUid(String uid);

    Optional<RobotTeamDbo> getRobotTeamByTag(String tag);


    RankingPlayer[] getTopTenPlayers();

    void saveGameResult(GameStatus status);

    void savePlayerResult(String name, int goals, int points);

    void saveRobotStatistics(Robot robot);

    List<RobotStatistics> getRobotStatisticsForToday();

    int getMatchCountForToday();

    int getMostGoalsPerMatchForToday();

    int addPlayer(String name);

    long getFastestGoalForToday();

    void saveNewGoal(int playerID, long time);
}
