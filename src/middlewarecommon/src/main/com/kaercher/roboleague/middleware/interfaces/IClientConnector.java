package com.kaercher.roboleague.middleware.interfaces;

import com.kaercher.roboleague.middleware.model.PushCommandReceiver;

public interface IClientConnector {

    void clientConnected(PushCommandReceiver receiver);

}
