package com.kaercher.roboleague.middleware.interfaces;

import com.kaercher.roboleague.middleware.model.Robot;
import com.kaercher.roboleague.middleware.model.RobotStatistics;

import java.util.List;
import java.util.Optional;

public interface IRobotController {

    void moveForward(String uid, int distance, int speed);

    void moveLeft(String uid, int angle, int speed);

    void moveRight(String uid, int angle, int speed);

    void rotate(String uid, long milliseconds);

    boolean activateItem(String uid, int itemSlot);

    List<Robot> getRobots();

    Optional<Robot> getRobotById(String uid);

    RobotStatistics getRobotStatistics(String uid);

    RobotStatistics getRobotStatistics(Robot robot);

}
