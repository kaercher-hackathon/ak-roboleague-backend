package com.kaercher.roboleague.middleware.interfaces;

import com.kaercher.roboleague.middleware.model.PushCommand;
import com.kaercher.roboleague.middleware.model.PushCommandReceiver;

public interface IPushConnector {

    void sendCommand(PushCommandReceiver receiver, PushCommand command);

    void disconnectClients();

    void disconnectClient(PushCommandReceiver receiver);

    void start();

    void close();
}
