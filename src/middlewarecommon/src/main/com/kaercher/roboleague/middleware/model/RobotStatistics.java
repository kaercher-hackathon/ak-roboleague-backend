package com.kaercher.roboleague.middleware.model;

public class RobotStatistics {

    private int distanceDriven;
    private int cleaningTime;
    private int areaCleaned;


    public RobotStatistics(int distanceDriven, int cleaningTime, int areaCleaned) {
        this.distanceDriven = distanceDriven;
        this.cleaningTime = cleaningTime;
        this.areaCleaned = areaCleaned;
    }

    public int getDistanceDriven() {
        return distanceDriven;
    }

    public void setDistanceDriven(int distanceDriven) {
        this.distanceDriven = distanceDriven;
    }

    public int getCleaningTime() {
        return cleaningTime;
    }

    public void setCleaningTime(int cleaningTime) {
        this.cleaningTime = cleaningTime;
    }

    public int getAreaCleaned() {
        return areaCleaned;
    }

    public void setAreaCleaned(int areaCleaned) {
        this.areaCleaned = areaCleaned;
    }
}
