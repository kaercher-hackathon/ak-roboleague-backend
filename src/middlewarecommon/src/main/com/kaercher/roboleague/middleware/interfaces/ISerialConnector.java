package com.kaercher.roboleague.middleware.interfaces;


import java.io.IOException;

public interface ISerialConnector {

    ISerialListener addRowListener(long time, ISerialListener task);

    void removeRowListener(long time);

    void connect() throws IOException, InterruptedException;

    void disconnect();

    void sendMessage(int arduinoID, String message);

}
