package com.kaercher.roboleague.middleware.model;

import java.util.Date;
import java.util.Optional;

public class GameItem {


    public enum ItemType {
        DOUBLE_GOAL,
        FREEZE,
        DIZZY,
        DONG;
    }

    public final static long DOUBLE_GOAL_DURATOIN = 60000L; // 60sec
    public final static long FREEZE_DURATION = 5000L; // 5sec
    public final static long DIZZY_DURATOIN = 3000L; // 3sec

    private final ItemType type;

    private Date activationTime;

    public GameItem(ItemType type) {
        this.type = type;
    }

    public ItemType getType() {
        return type;
    }

    public Optional<Date> getActivationTime() {
        return Optional.ofNullable(activationTime);
    }

    public void setActivationTime(Date activationTime) {
        this.activationTime = activationTime;
    }

    public long getRemaining() {
        long duration = getDuration();

        if (this.getActivationTime().isPresent()) {
            Date now = new Date();
            long timeDiff = (now.getTime() - this.getActivationTime().get().getTime());
            return duration - timeDiff;
        } else {
            return duration;
        }
    }

    public long getDuration() {
        switch (type) {
            case FREEZE:
                return GameItem.FREEZE_DURATION;
            case DIZZY:
                return GameItem.DIZZY_DURATOIN;
            case DOUBLE_GOAL:
                return GameItem.DOUBLE_GOAL_DURATOIN;
        }
        return 0;
    }
}
