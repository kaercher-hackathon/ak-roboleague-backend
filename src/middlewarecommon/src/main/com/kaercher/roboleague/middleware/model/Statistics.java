package com.kaercher.roboleague.middleware.model;

public class Statistics {

    public long fastestGoal = 0;

    public int mostGoals = 0;

    public long timeOfCleaning = 0;

    public int cleanedArea = 0;

}
