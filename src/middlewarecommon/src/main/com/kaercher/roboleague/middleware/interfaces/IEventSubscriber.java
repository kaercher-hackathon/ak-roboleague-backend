package com.kaercher.roboleague.middleware.interfaces;


public interface IEventSubscriber {

    void listen(String event, Object payload);
}
