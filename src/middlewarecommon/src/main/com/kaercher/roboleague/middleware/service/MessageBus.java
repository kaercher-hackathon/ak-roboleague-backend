package com.kaercher.roboleague.middleware.service;


import com.kaercher.roboleague.middleware.model.GameStatus;
import com.kaercher.roboleague.middleware.model.PushCommand;

public class MessageBus {

    private final static MessageBus INSTANCE = new MessageBus();

    private GameStatus gameStatus;

    private MessageBus() {
    }

    public static MessageBus getInstance() {
        return INSTANCE;
    }

    public void setGameStatus(GameStatus status) {
        this.gameStatus = status;
    }

    public void sendToHome(PushCommand command) {
        if (gameStatus != null)
            this.gameStatus.getHomePushCommandReceiver().ifPresent(receiver -> receiver.sendCommand(command));
    }

    public void sendToGuest(PushCommand command) {
        if (gameStatus != null)
            this.gameStatus.getGuestPushCommandReceiver().ifPresent(receiver -> receiver.sendCommand(command));
    }

    public void sendBoth(PushCommand command) {
        sendToHome(command);
        sendToGuest(command);
    }
}
