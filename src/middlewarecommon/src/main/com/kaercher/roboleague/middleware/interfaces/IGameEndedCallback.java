package com.kaercher.roboleague.middleware.interfaces;

public interface IGameEndedCallback {

    void gameEnded();
}
