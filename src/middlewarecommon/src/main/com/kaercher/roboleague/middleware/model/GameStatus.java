package com.kaercher.roboleague.middleware.model;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class GameStatus {

    public final long GAME_TIME = 360000;//12000; //360000; // 6 minutes

    public List<RfidReader> getReaderList() {
        return readerList;
    }

    public enum Status {
        PREGAME,
        COUNTDOWN,
        PLAYING,
        FINISHED,
        BREAK
    }

    private long remainingTime;
    private int countdownTime;
    private Status status;

    private Player homePlayer;
    private int homeGoals;
    private int homePoints;
    private List<GameItem> homeItems;
    private List<GameItem> activeHomeItems;

    private Player guestPlayer;
    private int guestGoals;
    private int guestPoints;
    private List<GameItem> guestItems;
    private List<GameItem> activeGuestItems;

    private List<RfidReader> readerList;

    public GameStatus(long playTime) {
        this.initDefaultValues();
        this.remainingTime = playTime;
    }

    public GameStatus() {
        this.initDefaultValues();
    }

    private void initDefaultValues() {
        this.remainingTime = GAME_TIME;
        this.countdownTime = 0;
        this.status = Status.PREGAME;

        this.homeGoals = 0;
        this.homePoints = 0;
        this.homePlayer = new Player();
        this.homePlayer.setName("Player 1");
        this.homeItems = new ArrayList<>();
        this.activeHomeItems = new ArrayList<>();

        this.guestGoals = 0;
        this.guestPoints = 0;
        this.guestPlayer = new Player();
        this.guestPlayer.setName("Player 2");
        this.guestItems = new ArrayList<>();
        this.activeGuestItems = new ArrayList<>();

        this.readerList = new ArrayList<>();
    }

    /* GETTER **/

    public int getHomePlayerID() {
        return this.homePlayer.getDbID();
    }

    public int getGuestPlayerID() {
        return this.guestPlayer.getDbID();
    }

    public long getRemainingTime() {
        return remainingTime;
    }

    public Status getStatus() {
        return status;
    }

    public Optional<Robot> getHomeRobot() {
        return Optional.ofNullable(this.homePlayer.getRobot());
    }

    public String getHomeName() {
        return this.homePlayer.getName();
    }

    public Optional<PushCommandReceiver> getHomePushCommandReceiver() {
        return Optional.ofNullable(this.homePlayer.getPushCommandReceiver());
    }

    public int getHomeGoals() {
        return homeGoals;
    }

    public int getHomePoints() {
        return homePoints;
    }

    public List<GameItem> getHomeItemsSnapshot() {
        return new ArrayList<>(homeItems);
    }

    public List<GameItem> getActiveHomeItemsSnapshot() {
        return new ArrayList<>(activeHomeItems);
    }

    public Optional<Robot> getGuestRobot() {
        return Optional.ofNullable(this.guestPlayer.getRobot());
    }

    public String getGuestName() {
        return this.guestPlayer.getName();
    }

    public Optional<PushCommandReceiver> getGuestPushCommandReceiver() {
        return Optional.ofNullable(this.guestPlayer.getPushCommandReceiver());
    }

    public int getGuestGoals() {
        return guestGoals;
    }

    public int getGuestPoints() {
        return guestPoints;
    }

    public List<GameItem> getGuestItemsSnapshot() {
        return new ArrayList<>(guestItems);
    }

    public List<GameItem> getActiveGuestItemsSnapshot() {
        return new ArrayList<>(activeGuestItems);
    }

    public int getCountdownTime() {
        return this.countdownTime;
    }

    /* END GETTER **/

    /* SETTER **/
    public void setHomePlayerID(int playerId) {
        this.homePlayer.setDbID(playerId);
    }

    public void setGuestPlayerID(int playerId) {
        this.guestPlayer.setDbID(playerId);
    }

    public void setRemainingTime(long remainingTime) {
        this.remainingTime = remainingTime;
    }

    public void decreaseRemainingTime(long time) {
        this.remainingTime = this.remainingTime - time;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public void setHomeRobot(Robot homeRobot) {
        Optional<Robot> robot = Optional.ofNullable(this.homePlayer.getRobot());
        if (!robot.isPresent())
            this.homePlayer.setRobot(homeRobot);
    }

    public void setHomeRobotForce(Robot homeRobot) {
        this.homePlayer.setRobot(homeRobot);
    }

    public void setHomeName(String homeName) {
        if (this.status == Status.PREGAME)
            this.homePlayer.setName(homeName);
    }

    public void setHomePushReceiver(PushCommandReceiver receiver) {
        this.homePlayer.setPushCommandReceiver(receiver);
    }

    public void setHomeGoals(int homeGoals) {
        this.homeGoals = homeGoals;
    }

    public void setHomePoints(int homePoints) {
        if (this.status == Status.PLAYING || this.status == Status.FINISHED)
            this.homePoints = homePoints;
    }

    public void setGuestRobot(Robot guestRobot) {
        Optional<Robot> robot = Optional.ofNullable(this.guestPlayer.getRobot());
        if (!robot.isPresent())
            this.guestPlayer.setRobot(guestRobot);
    }

    public void setGuestRobotForce(Robot guestRobot) {
        this.guestPlayer.setRobot(guestRobot);
    }

    public void setGuestName(String guestName) {
        if (this.status == Status.PREGAME)
            this.guestPlayer.setName(guestName);
    }

    public void setGuestPushReceiver(PushCommandReceiver receiver) {
        this.guestPlayer.setPushCommandReceiver(receiver);
    }

    public void setGuestGoals(int guestGoals) {
        this.guestGoals = guestGoals;
    }

    public void setGuestPoints(int guestPoints) {
        if (this.status == Status.PLAYING || this.status == Status.FINISHED)
            this.guestPoints = guestPoints;
    }

    public void setCountdownTime(int countdownTime) {
        this.countdownTime = countdownTime;
    }

    /*  END SETTER **/

    public void addHomeItem(GameItem item) {
        if (this.homeItems.size() < 2) {
            this.homeItems.add(item);
        }
    }

    public void addActiveHomeItem(GameItem item) {
        if (this.activeHomeItems.size() < 1) {
            this.activeHomeItems.add(item);
        }
    }

    public void addGuestItem(GameItem item) {
        if (this.guestItems.size() < 2) {
            this.guestItems.add(item);
        }
    }

    public void addActiveGuestItem(GameItem item) {
        if (this.activeGuestItems.size() < 1) {
            this.activeGuestItems.add(item);
        }
    }

    public void addHomePoints(int value) {
        this.homePoints += value;
    }

    public void addGuestPoints(int value) {
        this.guestPoints += value;
    }

    public void removeHomeItem(int index) {
        if (this.homeItems.size() > index && this.homeItems.size() > 0)
            this.homeItems.remove(index);
    }

    public void removeActiveHomeItem(int index) {
        if (this.activeHomeItems.size() > index && this.activeHomeItems.size() > 0)
            this.activeHomeItems.remove(index);
    }

    public void removeActiveHomeItem(GameItem item) {
        this.activeHomeItems.remove(item);
    }

    public void removeGuestItem(int index) {
        if (this.guestItems.size() > index && this.guestItems.size() > 0)
            this.guestItems.remove(index);
    }

    public void removeActiveGuestItem(int index) {
        if (this.activeGuestItems.size() > index && this.activeGuestItems.size() > 0)
            this.activeGuestItems.remove(index);
    }

    public void removeActiveGuestItem(GameItem item) {
        this.activeGuestItems.remove(item);
    }

    public void dispose() {
        this.homePlayer.setPushCommandReceiver(null);
        this.homePlayer.setRobot(null);
        this.guestPlayer.setPushCommandReceiver(null);
        this.guestPlayer.setRobot(null);
    }

}
