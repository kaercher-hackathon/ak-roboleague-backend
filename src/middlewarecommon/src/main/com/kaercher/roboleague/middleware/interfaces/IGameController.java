package com.kaercher.roboleague.middleware.interfaces;

import com.kaercher.roboleague.middleware.model.GameStatus;
import com.kaercher.roboleague.middleware.model.PushCommandReceiver;
import com.kaercher.roboleague.middleware.model.Robot;

public interface IGameController {

    boolean activateItem(Robot r, int itemSlot);

    GameStatus getGameStatus();

    void setHomeTeamName(String name);

    void setGuestTeamName(String name);

    boolean newGame();

    boolean startGame();

    boolean pauseGame();

    boolean resumeGame();

    void setHomePushReceiver(PushCommandReceiver receiver);

    void setGuestPushReceiver(PushCommandReceiver receiver);

    void setHomeRobot(Robot robot);

    void setGuestRobot(Robot robot);

}
