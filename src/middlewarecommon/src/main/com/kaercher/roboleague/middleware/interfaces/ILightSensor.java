package com.kaercher.roboleague.middleware.interfaces;

import com.kaercher.roboleague.middleware.model.Robot.RobotTeam;

public interface ILightSensor {

    boolean objectPresent(RobotTeam team);
}
