package com.kaercher.roboleague.middleware.interfaces;

import com.kaercher.roboleague.middleware.model.Robot;
import com.kaercher.roboleague.middleware.model.RobotStatistics;

import java.io.IOException;
import java.util.List;

public interface IRobotConnector {

    void enableDirectMode(Robot r);

    void moveForward(Robot r, int distance, int speed);

    void moveLeft(Robot r, int angle, int speed);

    void moveRight(Robot r, int angle, int speed);

    void stop(Robot r);

    void rotate(Robot r, long milliseconds);

    int getBattery(Robot r);

    String getStatus(Robot r) throws IOException;

    List<String> getRobotIpsInNetwork();

    RobotStatistics getRobotStatistics(Robot r);

}
