package com.kaercher.roboleague.middleware.interfaces;

import com.kaercher.roboleague.middleware.model.RankingPlayer;
import com.kaercher.roboleague.middleware.model.Statistics;

public interface IMiddleware {

    IRobotController getRobotController();

    IGameController getGameController();

    IDataProvider getDataProvider();


    boolean clientSocketSubmit(String key, String uid);

    RankingPlayer[] getRanking();

    Statistics getStatistics();

    void addHomeGoal();

    void addGuestGoal();
}
