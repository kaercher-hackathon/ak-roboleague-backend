package com.kaercher.roboleague.middleware.model;

import java.util.HashMap;

public class PushCommand {

    public enum CommandType {
        GAME_START, // ?time=[xx]&countdown=[true/false] -- On Start & On Countdown Ends
        GAME_END, // ?scoreHome=[xx]&scoreGuest=[xx]&pointsHome=[xx]&pointsGuest=[xx]&winner=[home/guest] -- On End
        GAME_PAUSE, // On PLAYING
        TIME, // ?remaining=xxx  -- sekündlich, ab GAME_START bis GAME_END
        COUNTDOWN, // ?value=[xx] -- sekündlich während Countdown
        SCORE_CHANGED, // ?scoreHome=[xx]&scoreGuest=[xx]&scored=[home/guest] -- On PLAYING
        OPPONENT_ITEM_USE, // ?item=[type]&duration=[xx]&robot=[enabled/disabled] -- On PLAYING
        OPPONENT_ITEM_EXPIRED, // ?item=[type]&robot=[enabled/disabled] -- On PLAYING
        OWN_ITEM_EXPIRED, // ?item=[type] -- On PLAYING
        ITEM_COLLECTED, // ?item=[type] -- On PLAYING
        SOCKET_SESSION, // ?key=[sessionkey] -- On CONNECTION
        DENIED // ?reason=[reason] -- On CONNECTION
    }

    private final CommandType type;

    private final HashMap<String, Object> properties;

    public PushCommand(CommandType cmdType) {
        this.type = cmdType;
        this.properties = new HashMap<>();
    }

    public void addProperty(String key, Object value) {
        this.properties.put(key, value);
    }

    public CommandType getType() {
        return type;
    }

    public HashMap<String, Object> getProperties() {
        return new HashMap<>(this.properties);
    }

}
