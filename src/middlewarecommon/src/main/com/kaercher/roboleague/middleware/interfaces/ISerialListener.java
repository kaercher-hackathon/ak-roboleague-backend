package com.kaercher.roboleague.middleware.interfaces;

public interface ISerialListener {

    void messageArrived(String ttyName, int arduinoId, String message);

}
