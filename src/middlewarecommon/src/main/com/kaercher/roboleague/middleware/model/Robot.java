package com.kaercher.roboleague.middleware.model;


public class Robot {

    public enum RobotTeam {
        HOME,
        GUEST
    }

    private String uid;

    private String ip;

    private RobotTeam team;

    private boolean isInNetwork;

    private int battery;

    private String mode;

    private String charging;

    private RobotStatistics gameStartStatistics;

    private RobotStatistics gameEndStatistics;

    public Robot(String uid, String ip, RobotTeam team) {
        this.uid = uid;
        this.ip = ip;
        this.team = team;
    }

    public Robot() {
    }

    public String getUid() {
        return uid;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
        this.setInNetwork(true);
    }

    public RobotTeam getTeam() {
        return team;
    }

    public void setTeam(RobotTeam team) {
        this.team = team;
    }

    public boolean isInNetwork() {
        return isInNetwork;
    }

    public void setInNetwork(boolean inNetwork) {
        isInNetwork = inNetwork;
    }

    public boolean getInNetwork() {
        return isInNetwork;
    }

    public void setBattery(int value) {
        this.battery = value;
    }

    public int getBattery() {
        return this.battery;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getCharging() {
        return charging;
    }

    public void setCharging(String charging) {
        this.charging = charging;
    }

    public RobotStatistics getGameStartStatistics() {
        return gameStartStatistics;
    }

    public void setGameStartStatistics(RobotStatistics gameStartStatistics) {
        this.gameStartStatistics = gameStartStatistics;
    }

    public RobotStatistics getGameEndStatistics() {
        return gameEndStatistics;
    }

    public void setGameEndStatistics(RobotStatistics gameEndStatistics) {
        this.gameEndStatistics = gameEndStatistics;
    }
}
