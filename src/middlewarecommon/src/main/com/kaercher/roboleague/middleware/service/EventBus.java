package com.kaercher.roboleague.middleware.service;

import com.kaercher.roboleague.middleware.interfaces.IEventSubscriber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;

public class EventBus {

    private final static EventBus instance = new EventBus();
    private final static Logger LOG = LoggerFactory.getLogger(EventBus.class);

    private List<IEventSubscriber> subscriber = new ArrayList<>();

    private EventBus() {
    }

    public static EventBus getInstance() {
        return instance;
    }


    public void addSubscriber(IEventSubscriber subscriber) {
        this.subscriber.add(subscriber);
    }

    public void removeSubscriber(IEventSubscriber subscriber) {
        this.subscriber.remove(subscriber);
    }

    public void emmit(String event, Object payload) {
        LOG.info("EVENT: " + event, payload);
        this.subscriber.forEach(sub -> sub.listen(event, payload));
    }

}
