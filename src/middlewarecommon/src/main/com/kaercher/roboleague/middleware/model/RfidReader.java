package com.kaercher.roboleague.middleware.model;


import com.kaercher.roboleague.middleware.interfaces.ISerialConnector;

public class RfidReader {

    public static final long READER_TIMEOUT = 30000L;

    public enum Position {
        TOP_LEFT,
        TOP_RIGHT,
        TOP_CENTER,
        BOTTOM_CENTER,
        BOTTOM_LEFT,
        BOTTOM_RIGHT;
    }

    private final int arduinoID;

    private final Position position;

    private final ISerialConnector connector;

    private long timeout = 0L;


    public RfidReader(int arduinoID, Position position, ISerialConnector connector) {
        this.arduinoID = arduinoID;
        this.position = position;
        this.connector = connector;
    }

    public int getArduinoID() {
        return arduinoID;
    }

    public Position getPosition() {
        return position;
    }

    public long getTimeout() {
        return timeout;
    }

    public void setTimeout(long timeout) {
        this.timeout = timeout;
    }

    public void decreaseTimeout(long value) {
        if (this.timeout <= 0) {
            this.connector.sendMessage(this.arduinoID, "LIGHT_ON");
        }

        this.timeout -= value;
        this.timeout = this.timeout < 0 ? this.timeout = 0 : this.timeout;
    }

}
