package com.kaercher.roboleague.middleware.serial.http;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class HttpClient {

    private final static Logger LOG = LoggerFactory.getLogger(HttpClient.class);

    public static String get(String url) throws IOException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(url);
        LOG.debug("Request GET: " + request.getURI().toString());
        HttpResponse result = httpClient.execute(request);
        return EntityUtils.toString(result.getEntity(), "UTF-8");
    }

    public static void put(String url, String body) throws IOException {
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        HttpPut request = new HttpPut(url);
        request.setEntity(new StringEntity(body, "utf-8"));
        LOG.debug("Request GET: " + request.getURI().toString());
        HttpResponse result = httpClient.execute(request);
    }
}