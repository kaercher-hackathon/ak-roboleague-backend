package com.kaercher.roboleague.middleware.serial.http;


import com.kaercher.roboleague.middleware.service.EventBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Date;

public class SerialConnection extends Thread {

    private final static Logger LOG = LoggerFactory.getLogger(SerialConnection.class);

    private final String TTY_NAME;
    private final String BASE_URL;
    private final SerialEventEmitter EVENT_EMITTER;

    private boolean shouldStop = false;
    private int arduinoID = -1;


    public enum Light {
        OFF,
        ON
    }

    public SerialConnection(String baseUrl, String ttyName, SerialEventEmitter eventEmitter) {
        this.TTY_NAME = ttyName;
        this.BASE_URL = baseUrl;
        this.EVENT_EMITTER = eventEmitter;
    }

    public void end() {
        this.shouldStop = true;
    }

    @Override
    public void run() {
        Date lastRunDate = new Date();
        while (!this.shouldStop) {
            Date startRun = new Date();
            long lastRunDiff = startRun.getTime() - lastRunDate.getTime();

            this.handleRequest();

            Date endRun = new Date();
            long runtimeDiff = endRun.getTime() - startRun.getTime();
            long sleepTime = runtimeDiff >= 50L ? 0 : 50L - runtimeDiff;
            lastRunDate = startRun;
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                LOG.error(e.getMessage(), e);
            }
        }
    }

    private void handleRequest() {

        try {
            String result = HttpClient.get(BASE_URL + "/" + TTY_NAME + "/out");
            if (result.startsWith("arduino:")) {
                this.handleMessage(result);
            }
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    private void handleMessage(String message) {
        String[] parts = message.split(":");
        if (parts[1].equals("init")) {
            this.arduinoID = Integer.parseInt(parts[2].trim());
            EventBus.getInstance().emmit("arduino_init", arduinoID);
            return;
        }
        if (parts[1].equals("nfc")) {
            String payload = parts[3];
            this.arduinoID = Integer.parseInt(parts[2]);
            this.EVENT_EMITTER.inform(this.TTY_NAME, this.arduinoID, payload.trim());
        }
    }

    public void setLight(Light light) {
        String message = "";
        if (light == Light.ON)
            message = "1\n";
        if (light == Light.OFF) {
            message = "0\n";
        }

        try {
            HttpClient.put(BASE_URL + "/" + TTY_NAME + "/in", message);
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    public int getArduinoID() {
        return arduinoID;
    }
}
