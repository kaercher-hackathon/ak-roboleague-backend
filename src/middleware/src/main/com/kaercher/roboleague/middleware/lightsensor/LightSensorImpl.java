package com.kaercher.roboleague.middleware.lightsensor;

import com.kaercher.roboleague.middleware.interfaces.ILightSensor;
import com.kaercher.roboleague.middleware.model.Robot.RobotTeam;
import com.pi4j.io.gpio.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.GUEST;
import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.HOME;

public class LightSensorImpl implements ILightSensor {

    private static final Logger LOG = LoggerFactory.getLogger(LightSensorImpl.class);
    private static final GpioController gpio = GpioFactory.getInstance();

    private GpioPinDigitalInput homeTeamSensor =
            gpio.provisionDigitalInputPin(RaspiPin.GPIO_01); //Pin 12

    private GpioPinDigitalInput guestTeamSensor =
            gpio.provisionDigitalInputPin(RaspiPin.GPIO_04); //Pin 16

    @Override
    public boolean objectPresent(RobotTeam team) {
        if (team == HOME) {
            LOG.debug("HomeBarrier: " + homeTeamSensor.getState().getName());
            return homeTeamSensor.getState() == PinState.HIGH;
        }
        if (team == GUEST) {
            LOG.debug("GuestBarrier: " + guestTeamSensor.getState().getName());
            return guestTeamSensor.getState() == PinState.HIGH;
        }
        return false;
    }
}
