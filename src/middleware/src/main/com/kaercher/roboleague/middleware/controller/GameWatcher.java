package com.kaercher.roboleague.middleware.controller;


import com.kaercher.roboleague.middleware.constants.GoalConstants;
import com.kaercher.roboleague.middleware.factory.PushCommandFactory;
import com.kaercher.roboleague.middleware.interfaces.IGameEndedCallback;
import com.kaercher.roboleague.middleware.interfaces.ILightSensor;
import com.kaercher.roboleague.middleware.model.GameItem;
import com.kaercher.roboleague.middleware.model.GameStatus;
import com.kaercher.roboleague.middleware.model.Robot;
import com.kaercher.roboleague.middleware.model.Robot.RobotTeam;
import com.kaercher.roboleague.middleware.service.EventBus;
import com.kaercher.roboleague.middleware.service.MessageBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

import static com.kaercher.roboleague.middleware.model.GameItem.ItemType.*;
import static com.kaercher.roboleague.middleware.model.GameStatus.Status.BREAK;
import static com.kaercher.roboleague.middleware.model.GameStatus.Status.PLAYING;
import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.GUEST;
import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.HOME;

public class GameWatcher extends Thread {

    private final static Logger LOG = LoggerFactory.getLogger(GameWatcher.class);

    private final GameStatus gameStatus;
    private final IGameEndedCallback endGameCallback;
    private boolean shouldStop = false;
    private final ILightSensor lightSensor;

    GameWatcher(GameStatus gameStatus, ILightSensor lightSensor, IGameEndedCallback callback) {
        this.gameStatus = gameStatus;
        this.endGameCallback = callback;
        this.lightSensor = lightSensor;
    }


    @Override
    public void run() {
        Date lastRunDate = new Date();
        while (!shouldStop) {
            Date startRun = new Date();
            long lastRunDiff = startRun.getTime() - lastRunDate.getTime();
            this.handlePlayTimeChanges(lastRunDiff);

            this.handleExpiredItems();

            this.handleReaderTimeoutChanges(lastRunDiff);

            this.handleGoals();

            Date endRun = new Date();
            long runtimeDiff = endRun.getTime() - startRun.getTime();
            long sleepTime = runtimeDiff >= 100L ? 0 : 100L - runtimeDiff;
            lastRunDate = startRun;
            try {
                Thread.sleep(sleepTime);
            } catch (InterruptedException e) {
                LOG.error(e.getMessage(), e);
            }
        }
    }

    private void handleGoals() {
        if (this.gameStatus.getStatus() != PLAYING)
            return;

        if (this.lightSensor.objectPresent(HOME)) {
            this.gameStatus.setStatus(BREAK);
            int multiplier = this.gameStatus.getActiveHomeItemsSnapshot().size() > 0 && this.gameStatus.getActiveHomeItemsSnapshot().get(0).getType() == DOUBLE_GOAL ? GoalConstants.SCORE_MULTIPLIER : 1;
            this.gameStatus.addHomePoints(GoalConstants.SCORE_GOAL * multiplier);
            this.gameStatus.setHomeGoals(this.gameStatus.getHomeGoals() + 1);
            if (this.gameStatus.getHomeGoals() % 3 == 0) {
                this.gameStatus.addHomePoints(GoalConstants.HATTRICK);
            }
            MessageBus.getInstance().sendBoth(PushCommandFactory.getScoreChanged(this.gameStatus.getHomeGoals(), this.gameStatus.getGuestGoals(), HOME));
            this.removeItemsAfterGoal();
            this.emitGoal(HOME);
        } else if (this.lightSensor.objectPresent(GUEST)) {
            this.gameStatus.setStatus(BREAK);
            int multiplier = this.gameStatus.getActiveGuestItemsSnapshot().size() > 0 && this.gameStatus.getActiveGuestItemsSnapshot().get(0).getType() == DOUBLE_GOAL ? GoalConstants.SCORE_MULTIPLIER : 1;
            this.gameStatus.addGuestPoints(GoalConstants.SCORE_GOAL * multiplier);
            this.gameStatus.setGuestGoals(this.gameStatus.getGuestGoals() + 1);
            if (this.gameStatus.getGuestGoals() % 3 == 0) {
                this.gameStatus.addGuestPoints(GoalConstants.HATTRICK);
            }
            MessageBus.getInstance().sendBoth(PushCommandFactory.getScoreChanged(this.gameStatus.getHomeGoals(), this.gameStatus.getGuestGoals(), GUEST));
            this.removeItemsAfterGoal();
            this.emitGoal(GUEST);
        }
    }

    private void removeItemsAfterGoal() {
        this.gameStatus.removeActiveHomeItem(0);
        this.gameStatus.removeActiveGuestItem(0);
    }

    private void emitGoal(RobotTeam team) {
        if (team == HOME)
            EventBus.getInstance().emmit("home_goal_scored", null);
        if (team == GUEST)
            EventBus.getInstance().emmit("guest_goal_scored", null);
    }

    private void handleReaderTimeoutChanges(long lastRunDiff) {
        this.gameStatus.getReaderList().forEach(reader -> reader.decreaseTimeout(lastRunDiff));
    }

    private void handlePlayTimeChanges(long lastRunDiff) {
        if (this.gameStatus.getStatus() == GameStatus.Status.PLAYING) {
            this.gameStatus.decreaseRemainingTime(lastRunDiff);
            MessageBus.getInstance().sendBoth(PushCommandFactory.getGameTime(gameStatus.getRemainingTime()));
        }
        if (this.gameStatus.getRemainingTime() <= 0) {
            this.gameStatus.setStatus(GameStatus.Status.FINISHED);
            this.gameStatus.setRemainingTime(0);
            this.shouldStop = true;

            MessageBus.getInstance().sendBoth(PushCommandFactory.getGameEnded(
                    gameStatus.getHomeGoals(), gameStatus.getGuestGoals(),
                    gameStatus.getHomePoints(), gameStatus.getGuestPoints()));

            this.endGameCallback.gameEnded();
        }
    }

    private void handleExpiredItems() {
        this.gameStatus.getActiveHomeItemsSnapshot()
                .forEach(gameItem -> {
                    if (itemExpired(gameItem)) {
                        MessageBus.getInstance().sendToHome(PushCommandFactory.getOwnItemExpired(gameItem.getType()));
                        if (gameItem.getType() == DIZZY || gameItem.getType() == FREEZE)
                            MessageBus.getInstance().sendToGuest(PushCommandFactory.getOpponentItemExpired(gameItem.getType(), true));
                        this.gameStatus.removeActiveHomeItem(gameItem);
                    }
                });

        this.gameStatus.getActiveGuestItemsSnapshot()
                .forEach(gameItem -> {
                    if (itemExpired(gameItem))
                        MessageBus.getInstance().sendToGuest(PushCommandFactory.getOwnItemExpired(gameItem.getType()));
                    if (gameItem.getType() == DIZZY || gameItem.getType() == FREEZE)
                        MessageBus.getInstance().sendToHome(PushCommandFactory.getOpponentItemExpired(gameItem.getType(), true));
                    this.gameStatus.removeActiveGuestItem(gameItem);
                });
    }

    private boolean itemExpired(GameItem gameItem) {
        return gameItem.getRemaining() <= 0;
    }
}
