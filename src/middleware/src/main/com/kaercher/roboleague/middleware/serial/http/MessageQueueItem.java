package com.kaercher.roboleague.middleware.serial.http;

public class MessageQueueItem {

    private final int arduinoID;
    private final String message;

    public MessageQueueItem(int arduinoID, String message) {
        this.arduinoID = arduinoID;
        this.message = message;
    }

    public int getArduinoID() {
        return arduinoID;
    }

    public String getMessage() {
        return message;
    }
}
