package com.kaercher.roboleague.middleware.serial.http;


public interface SerialEventEmitter {

    void inform(String ttyName, int arduinoId, String message);
}
