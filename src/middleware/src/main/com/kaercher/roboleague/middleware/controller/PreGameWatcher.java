package com.kaercher.roboleague.middleware.controller;

import com.kaercher.roboleague.middleware.model.GameStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class PreGameWatcher extends Thread {

    private final static Logger LOG = LoggerFactory.getLogger(PreGameWatcher.class);

    private GameStatus gameStatus;
    private Runnable runnable;
    private boolean shouldCancel = false;

    PreGameWatcher(GameStatus gameStatus, Runnable runnable) {
        this.gameStatus = gameStatus;
        this.runnable = runnable;
    }

    @Override
    public void run() {
        while (!shouldCancel) {
            if (this.gameStatus.getHomeRobot().isPresent() || this.gameStatus.getGuestRobot().isPresent()) {
                this.runnable.run();
                this.shouldCancel = true;
            }
            try {
                Thread.sleep(100L);
            } catch (InterruptedException e) {
                LOG.error(e.getMessage(), e);
            }

        }
    }

    public void cancel() {
        this.shouldCancel = true;
    }
}
