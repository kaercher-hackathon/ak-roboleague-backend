package com.kaercher.roboleague.middleware.lightsensor;

import com.kaercher.roboleague.middleware.interfaces.ILightSensor;
import com.kaercher.roboleague.middleware.model.Robot.RobotTeam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.GUEST;
import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.HOME;

public class AlwaysOnLightSensorImpl implements ILightSensor {

    private static final Logger LOG = LoggerFactory.getLogger(AlwaysOnLightSensorImpl.class);

    @Override
    public boolean objectPresent(RobotTeam team) {
        if (team == HOME) {
            return false;
        }
        if (team == GUEST) {
            return false;
        }
        return false;
    }
}
