package com.kaercher.roboleague.middleware.lightsensor;


import com.kaercher.roboleague.middleware.interfaces.ILightSensor;

public class ApiTriggeredGoalService {

    private final static ApiTriggeredGoalService instance = new ApiTriggeredGoalService();
    private ApiTriggeredLightSensorImpl lightSensorImpl;

    private ApiTriggeredGoalService() {

    }

    public static ApiTriggeredGoalService getInstance() {
        return instance;
    }

    public ApiTriggeredLightSensorImpl getLightSensorImpl() {
        return lightSensorImpl;
    }

    public void setLightSensorImpl(ApiTriggeredLightSensorImpl lightSensorImpl) {
        this.lightSensorImpl = lightSensorImpl;
    }

    public void setHomeGoal() {
        if (this.lightSensorImpl != null) {
            this.lightSensorImpl.setHomeGoal();
        }
    }

    public void setGuestGoal() {
        if (this.lightSensorImpl != null) {
            this.lightSensorImpl.setGuestGoal();
        }
    }
}
