package com.kaercher.roboleague.middleware.lightsensor;

import com.kaercher.roboleague.middleware.interfaces.ILightSensor;
import com.kaercher.roboleague.middleware.model.Robot;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.GUEST;
import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.HOME;

public class WindowsMockLightSensorImpl implements ILightSensor {

    private static final Logger LOG = LoggerFactory.getLogger(WindowsMockLightSensorImpl.class);


    @Override
    public boolean objectPresent(Robot.RobotTeam team) {
        if (team == HOME) {
            return new Date().getMinutes() % 2 == 0;
        }
        if (team == GUEST) {
            return new Date().getMinutes() % 3 == 0;
        }
        return false;
    }
}