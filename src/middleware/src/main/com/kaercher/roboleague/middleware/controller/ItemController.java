package com.kaercher.roboleague.middleware.controller;


import com.kaercher.roboleague.middleware.dbo.RobotTeamDbo;
import com.kaercher.roboleague.middleware.factory.PushCommandFactory;
import com.kaercher.roboleague.middleware.interfaces.IDataProvider;
import com.kaercher.roboleague.middleware.interfaces.ISerialConnector;
import com.kaercher.roboleague.middleware.model.GameItem;
import com.kaercher.roboleague.middleware.model.GameStatus;
import com.kaercher.roboleague.middleware.model.RfidReader;
import com.kaercher.roboleague.middleware.model.RfidReader.Position;
import com.kaercher.roboleague.middleware.model.Robot.RobotTeam;
import com.kaercher.roboleague.middleware.service.EventBus;
import com.kaercher.roboleague.middleware.service.MessageBus;

import java.util.Date;
import java.util.Optional;

import static com.kaercher.roboleague.middleware.constants.ItemConstants.COLLECT_ITEM_POINTS;
import static com.kaercher.roboleague.middleware.constants.ItemConstants.DONG_POINTS;
import static com.kaercher.roboleague.middleware.model.GameItem.ItemType.DONG;
import static com.kaercher.roboleague.middleware.model.GameStatus.Status.PLAYING;
import static com.kaercher.roboleague.middleware.model.RfidReader.Position.*;

public class ItemController {

    private final long READER_TIMEOUT = 30000L;

    private RfidReader top_left;
    private RfidReader top_right;
    private RfidReader top_center;
    private RfidReader bottom_center;
    private RfidReader bottom_left;
    private RfidReader bottom_right;

    private GameStatus gameStatus;
    private IDataProvider dataProvider;
    private ISerialConnector serialConnector;

    private final long ID = new Date().getTime();

    public ItemController(GameStatus gameStatus, IDataProvider dbProvider, ISerialConnector serialConnector) {
        this.gameStatus = gameStatus;
        this.dataProvider = dbProvider;
        this.serialConnector = serialConnector;
        this.initReader();
        EventBus.getInstance().addSubscriber(this::handleEventSubscription);
        this.setReaderToGamestatus();
        this.initReaderLedState();
        this.serialConnector.addRowListener(ID, this::handleMessage);
    }

    private void initReader() {
        top_left = new RfidReader(1, TOP_LEFT, serialConnector);
        top_center = new RfidReader(2, TOP_CENTER, serialConnector);
        top_right = new RfidReader(3, TOP_RIGHT, serialConnector);
        bottom_left = new RfidReader(4, BOTTOM_LEFT, serialConnector);
        bottom_center = new RfidReader(5, BOTTOM_CENTER, serialConnector);
        bottom_right = new RfidReader(6, BOTTOM_RIGHT, serialConnector);
    }

    private void initReaderLedState() {
        serialConnector.sendMessage(top_left.getArduinoID(), "LIGHT_ON");
        serialConnector.sendMessage(top_center.getArduinoID(), "LIGHT_ON");
        serialConnector.sendMessage(top_right.getArduinoID(), "LIGHT_ON");
        serialConnector.sendMessage(bottom_left.getArduinoID(), "LIGHT_ON");
        serialConnector.sendMessage(bottom_center.getArduinoID(), "LIGHT_ON");
        serialConnector.sendMessage(bottom_right.getArduinoID(), "LIGHT_ON");
    }

    private void setReaderToGamestatus() {
        gameStatus.getReaderList().clear();
        gameStatus.getReaderList().add(top_left);
        gameStatus.getReaderList().add(top_center);
        gameStatus.getReaderList().add(top_right);
        gameStatus.getReaderList().add(bottom_left);
        gameStatus.getReaderList().add(bottom_center);
        gameStatus.getReaderList().add(bottom_right);
    }

    private void handleMessage(String ttyName, int arduinoId, String message) {
        if (gameStatus.getStatus() != PLAYING)
            return;

        RfidReader reader = getReaderByArduinoId(arduinoId);
        if (reader == null)
            return;

        if (reader.getTimeout() > 0)
            return;

        RobotTeamDbo robot = getRobotByMessage(message);
        if (robot == null)
            return;

        if (!isActiveRobot(gameStatus, robot))
            return;

        RobotTeam team = RobotTeam.valueOf(robot.team);

        GameItem item = getItemForPosition(reader.getPosition());
        if (item == null)
            return;
        if (item.getType() == DONG) {
            switch (team) {
                case HOME:
                    gameStatus.addHomePoints(DONG_POINTS);
                    break;
                case GUEST:
                    gameStatus.addGuestPoints(DONG_POINTS);
                    break;
            }
        } else {

            if (team == RobotTeam.HOME) {
                if (gameStatus.getHomeItemsSnapshot().size() >= 2)
                    return;
                gameStatus.addHomeItem(item);
                gameStatus.addHomePoints(COLLECT_ITEM_POINTS);
                MessageBus.getInstance().sendToHome(PushCommandFactory.getItemCollected(item.getType(), gameStatus.getHomeItemsSnapshot().size() - 1));
            } else {
                if (gameStatus.getGuestItemsSnapshot().size() >= 2)
                    return;
                gameStatus.addGuestItem(item);
                gameStatus.addGuestPoints(COLLECT_ITEM_POINTS);
                MessageBus.getInstance().sendToGuest(PushCommandFactory.getItemCollected(item.getType(), gameStatus.getHomeItemsSnapshot().size() - 1));
            }
        }
        reader.setTimeout(READER_TIMEOUT);
        serialConnector.sendMessage(arduinoId, "LIGHT_OFF");
    }

    private void handleEventSubscription(String event, Object payload) {
        if (event.equals("arduino_init")) {
            RfidReader reader = getReaderByArduinoId((int) payload);
            if (reader != null)
                serialConnector.sendMessage((int) payload, reader.getTimeout() > 0 ? "LIGHT_OFF" : "LIGHT_ON");
        }
    }

    private RobotTeamDbo getRobotByMessage(String message) {
        if (message.startsWith("en")) {
            String robotName = message.substring("en".length());
            Optional<RobotTeamDbo> robot = this.dataProvider.getRobotTeamByTag(robotName);
            if (robot.isPresent()) {
                return robot.get();
            }
        }
        return null;
    }

    private boolean isActiveRobot(GameStatus gameStatus, RobotTeamDbo robot) {
        if (robot.team.equals("HOME")) {
            if (!gameStatus.getHomeRobot().isPresent())
                return false;
            if (gameStatus.getHomeRobot().get().getUid().equals(robot.uid))
                return true;
        }

        if (robot.team.equals("GUEST")) {
            if (!gameStatus.getGuestRobot().isPresent())
                return false;
            return gameStatus.getGuestRobot().get().getUid().equals(robot.uid);
        }

        return false;
    }

    private RfidReader getReaderByArduinoId(int id) {
        switch (id) {
            case 1:
                return top_left;
            case 2:
                return top_center;
            case 3:
                return top_right;
            case 4:
                return bottom_left;
            case 5:
                return bottom_center;
            case 6:
                return bottom_right;
            default:
                return null;
        }
    }

    private GameItem getItemForPosition(Position position) {
        switch (position) {
            case TOP_LEFT:
                return new GameItem(GameItem.ItemType.DIZZY);
            case TOP_CENTER:
                return new GameItem(GameItem.ItemType.DOUBLE_GOAL);
            case TOP_RIGHT:
                return new GameItem(GameItem.ItemType.FREEZE);
            case BOTTOM_LEFT:
                return new GameItem(GameItem.ItemType.FREEZE);
            case BOTTOM_CENTER:
                return new GameItem(DONG);
            case BOTTOM_RIGHT:
                return new GameItem(GameItem.ItemType.DIZZY);
            default:
                return null;
        }
    }

    public void dispose() {
        top_left = null;
        top_center = null;
        top_right = null;
        bottom_left = null;
        bottom_center = null;
        bottom_right = null;
        this.serialConnector.removeRowListener(ID);
        EventBus.getInstance().removeSubscriber(this::handleEventSubscription);

    }
}
