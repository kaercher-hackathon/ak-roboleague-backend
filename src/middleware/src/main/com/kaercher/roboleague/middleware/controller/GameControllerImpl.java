package com.kaercher.roboleague.middleware.controller;

import com.kaercher.roboleague.middleware.MiddlewareImpl;
import com.kaercher.roboleague.middleware.constants.ItemConstants;
import com.kaercher.roboleague.middleware.factory.PushCommandFactory;
import com.kaercher.roboleague.middleware.interfaces.IGameController;
import com.kaercher.roboleague.middleware.interfaces.IGameEndedCallback;
import com.kaercher.roboleague.middleware.interfaces.ILightSensor;
import com.kaercher.roboleague.middleware.interfaces.ISerialConnector;
import com.kaercher.roboleague.middleware.lightsensor.AlwaysOnLightSensorImpl;
import com.kaercher.roboleague.middleware.lightsensor.ApiTriggeredLightSensorImpl;
import com.kaercher.roboleague.middleware.lightsensor.WindowsMockLightSensorImpl;
import com.kaercher.roboleague.middleware.model.GameItem;
import com.kaercher.roboleague.middleware.model.GameStatus;
import com.kaercher.roboleague.middleware.model.PushCommandReceiver;
import com.kaercher.roboleague.middleware.model.Robot;
import com.kaercher.roboleague.middleware.serial.HttpSerialConnector;
import com.kaercher.roboleague.middleware.service.EventBus;
import com.kaercher.roboleague.middleware.service.MessageBus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.kaercher.roboleague.middleware.factory.PushCommandFactory.getGamePaused;
import static com.kaercher.roboleague.middleware.model.GameItem.ItemType.DIZZY;
import static com.kaercher.roboleague.middleware.model.GameItem.ItemType.FREEZE;
import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.HOME;

public class GameControllerImpl implements IGameController, IGameEndedCallback {

    private final static Logger LOG = LoggerFactory.getLogger(GameControllerImpl.class);

    private final MiddlewareImpl middleware;
    private final ILightSensor lightSensor;
    private ISerialConnector serialConnector;
    private GameStatus gameStatus;

    private GameWatcher watcher;

    private ItemController itemController;
    private Timer blinkTimer = new Timer();


    public GameControllerImpl(MiddlewareImpl middleware) {
        this.middleware = middleware;

        /*if (System.getProperty("os.name").startsWith("Windows"))
            this.lightSensor = new WindowsMockLightSensorImpl();
        else {*/
        //this.lightSensor = new LightSensorImpl();
        //this.lightSensor = new AlwaysOnLightSensorImpl();
        this.lightSensor = new ApiTriggeredLightSensorImpl();
        //}

        this.serialConnector = new HttpSerialConnector();
        try {
            this.serialConnector.connect();
        } catch (IOException | InterruptedException e) {
            LOG.error(e.getMessage(), e);
        }

        EventBus.getInstance().addSubscriber(this::handleScoredGoal);

        this.createNewGame();
    }

    private void handleScoredGoal(String event, Object payload) {
        if (event.equals("home_goal_scored")) {
            this.middleware.getDataProvider().saveNewGoal(gameStatus.getHomePlayerID(), gameStatus.GAME_TIME - gameStatus.getRemainingTime());
        }

        if (event.equals("guest_goal_scored")) {
            this.middleware.getDataProvider().saveNewGoal(gameStatus.getGuestPlayerID(), gameStatus.GAME_TIME - gameStatus.getRemainingTime());
        }
    }


    @Override
    public boolean activateItem(Robot r, int itemSlot) {
        if (r.getTeam() == HOME) {
            return activateHomeItem(r, itemSlot);
        } else {
            return activateGuestItem(r, itemSlot);
        }
    }

    private boolean activateHomeItem(Robot robot, int itemSlot) {

        if (!gameStatus.getHomeRobot().isPresent()) {
            return false;
        }
        if (!robot.getUid().equals(gameStatus.getHomeRobot().get().getUid())) {
            return false;
        }

        List<GameItem> teamItems = gameStatus.getHomeItemsSnapshot();

        if (teamItems.size() < itemSlot + 1)
            return false;
        if (gameStatus.getActiveHomeItemsSnapshot().size() >= 1)
            return false;
        GameItem activatedItem = teamItems.get(itemSlot);

        this.gameStatus.removeHomeItem(itemSlot);
        this.gameStatus.addActiveHomeItem(activatedItem);
        this.gameStatus.addHomePoints(ItemConstants.USE_ITEM_POINTS);
        activatedItem.setActivationTime(new Date());

        boolean robotEnabled = activatedItem.getType() == FREEZE || activatedItem.getType() == DIZZY;
        MessageBus.getInstance().sendToGuest(PushCommandFactory.getOpponentUsingItem(activatedItem.getType(), activatedItem.getDuration(), robotEnabled));

        if (activatedItem.getType() == DIZZY)
            this.gameStatus.getGuestRobot().ifPresent(r -> this.middleware.getRobotController().rotate(r.getUid(), activatedItem.getDuration()));

        return true;
    }

    private boolean activateGuestItem(Robot robot, int itemSlot) {
        if (!gameStatus.getGuestRobot().isPresent()) {
            return false;
        }
        if (!robot.getUid().equals(gameStatus.getGuestRobot().get().getUid())) {
            return false;
        }

        List<GameItem> teamItems = gameStatus.getGuestItemsSnapshot();

        if (teamItems.size() < itemSlot + 1)
            return false;
        if (gameStatus.getActiveGuestItemsSnapshot().size() >= 1)
            return false;

        GameItem activatedItem = teamItems.get(itemSlot);
        this.gameStatus.removeGuestItem(itemSlot);
        this.gameStatus.addActiveGuestItem(activatedItem);
        this.gameStatus.addGuestPoints(ItemConstants.USE_ITEM_POINTS);
        activatedItem.setActivationTime(new Date());

        boolean robotEnabled = activatedItem.getType() == FREEZE || activatedItem.getType() == DIZZY;
        MessageBus.getInstance().sendToHome(PushCommandFactory.getOpponentUsingItem(activatedItem.getType(), activatedItem.getDuration(), robotEnabled));

        if (activatedItem.getType() == DIZZY)
            this.gameStatus.getHomeRobot().ifPresent(r -> this.middleware.getRobotController().rotate(r.getUid(), activatedItem.getDuration()));

        return true;
    }

    @Override
    public GameStatus getGameStatus() {
        return this.gameStatus;
    }

    @Override
    public void setHomeTeamName(String name) {
        this.gameStatus.setHomeName(name);
    }

    @Override
    public void setGuestTeamName(String name) {
        this.gameStatus.setGuestName(name);
    }

    @Override
    public boolean newGame() {
        if (this.gameStatus == null) {
            this.createNewGame();
            return true;
        } else if (this.gameStatus.getStatus() != GameStatus.Status.COUNTDOWN && this.gameStatus.getStatus() != GameStatus.Status.PLAYING) {
            this.gameStatus.dispose();
            this.createNewGame();
            return true;
        }
        return false;
    }

    private void createNewGame() {
        this.gameStatus = new GameStatus();
        MessageBus.getInstance().setGameStatus(this.gameStatus);
        this.middleware.resetSockets();
        this.itemController = new ItemController(this.gameStatus, this.middleware.getDataProvider(), this.serialConnector);
    }

    @Override
    public boolean startGame() {
        // TODO Check if 1/2 Connected


        if (this.gameStatus.getStatus() != GameStatus.Status.PREGAME) {
            return false;
        }
/*
        PreGameWatcher watcher = new PreGameWatcher(this.gameStatus, () -> {
  */
        this.blinkTimer.cancel();
        this.blinkTimer.purge();

        this.gameStatus.setHomePlayerID(this.middleware.getDataProvider().addPlayer(this.gameStatus.getHomeName()));
        this.gameStatus.setGuestPlayerID(this.middleware.getDataProvider().addPlayer(this.gameStatus.getGuestName()));

        this.gameStatus.setStatus(GameStatus.Status.COUNTDOWN);
        this.gameStatus.setCountdownTime(3);
        this.watcher = new GameWatcher(gameStatus, lightSensor, this);
        CountdownController.startCountdownAndWatcher(this.watcher, gameStatus);
        /*});
        watcher.start();
*/
        return true;
    }

    @Override
    public boolean pauseGame() {
        if (this.gameStatus.getStatus() != GameStatus.Status.PLAYING)
            return false;

        this.gameStatus.setStatus(GameStatus.Status.BREAK);
        MessageBus.getInstance().sendBoth(getGamePaused());
        return true;
    }

    @Override
    public boolean resumeGame() {
        if (this.gameStatus.getStatus() != GameStatus.Status.BREAK)
            return false;

        this.gameStatus.setStatus(GameStatus.Status.COUNTDOWN);
        this.gameStatus.setCountdownTime(3);
        CountdownController.startCountdown(gameStatus);

        return true;
    }

    @Override
    public void setHomePushReceiver(PushCommandReceiver receiver) {
        this.gameStatus.setHomePushReceiver(receiver);
    }

    @Override
    public void setGuestPushReceiver(PushCommandReceiver receiver) {
        this.gameStatus.setGuestPushReceiver(receiver);
    }

    @Override
    public void setHomeRobot(Robot robot) {
        this.gameStatus.setHomeRobot(robot);
    }

    @Override
    public void setGuestRobot(Robot robot) {
        this.gameStatus.setGuestRobot(robot);
    }

    @Override
    public void gameEnded() {
        //TODO write Result to DB
        //TODO End all processes for this round
        try {
            if (gameStatus.getHomeRobot().isPresent()) {
                Robot robot = gameStatus.getHomeRobot().get();
                robot.setGameEndStatistics(this.middleware.getRobotController().getRobotStatistics(robot));
                this.middleware.getDataProvider().saveRobotStatistics(robot);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage());
        }

        try {
            if (gameStatus.getGuestRobot().isPresent()) {
                Robot robot = gameStatus.getGuestRobot().get();
                robot.setGameEndStatistics(this.middleware.getRobotController().getRobotStatistics(robot));
                this.middleware.getDataProvider().saveRobotStatistics(robot);
            }
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }

        try {
            this.middleware.getDataProvider().saveGameResult(gameStatus);
            this.middleware.getDataProvider().savePlayerResult(gameStatus.getHomeName(), gameStatus.getHomeGoals(), gameStatus.getHomePoints());
            this.middleware.getDataProvider().savePlayerResult(gameStatus.getGuestName(), gameStatus.getGuestGoals(), gameStatus.getGuestPoints());
            this.itemController.dispose();
            this.itemController = null;
        } catch (Exception e) {
            LOG.error(e.getMessage(), e);
        }

        this.blinkTimer = new Timer();
        this.blinkTimer.scheduleAtFixedRate(new TimerTask() {
            int current = 1;

            @Override
            public void run() {
                serialConnector.sendMessage(current, "LIGHT_OFF");
                current = current == 6 ? 1 : current + 1;
                serialConnector.sendMessage(current, "LIGHT_ON");
            }
        }, 0, 200);

    }
}
