package com.kaercher.roboleague.middleware.factory;

import com.kaercher.roboleague.middleware.model.GameItem;
import com.kaercher.roboleague.middleware.model.PushCommand;
import com.kaercher.roboleague.middleware.model.Robot;

import static com.kaercher.roboleague.middleware.model.PushCommand.CommandType.*;
import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.GUEST;
import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.HOME;

public class PushCommandFactory {

    public static PushCommand getGameEnded(int scoreHome, int scoreGuest, int pointsHome, int pointsGuest) {
        String winner = "NONE";
        if (pointsHome > pointsGuest)
            winner = HOME.name();
        else if (pointsGuest > pointsHome)
            winner = GUEST.name();

        PushCommand cmd = new PushCommand(GAME_END);
        cmd.addProperty("scoreHome", scoreHome);
        cmd.addProperty("scoreGuest", scoreGuest);
        cmd.addProperty("pointsHome", pointsHome);
        cmd.addProperty("pointsGuest", pointsGuest);
        cmd.addProperty("winner", winner);
        return cmd;
    }

    public static PushCommand getGameTime(long gameTime) {
        PushCommand cmd = new PushCommand(TIME);
        cmd.addProperty("remaining", gameTime);
        return cmd;
    }

    public static PushCommand getDenied(String reason) {
        PushCommand cmd = new PushCommand(DENIED);
        cmd.addProperty("reason", reason);
        return cmd;
    }

    public static PushCommand getCountdown(int value) {
        PushCommand cmd = new PushCommand(COUNTDOWN);
        cmd.addProperty("value", value);
        return cmd;
    }

    public static PushCommand getGameStart(long gameTime, boolean countdown) {
        PushCommand cmd = new PushCommand(GAME_START);
        cmd.addProperty("time", gameTime);
        cmd.addProperty("countdown", countdown);
        return cmd;
    }

    public static PushCommand getScoreChanged(int scoreHome, int scoreGuest, Robot.RobotTeam team) {
        PushCommand cmd = new PushCommand(SCORE_CHANGED);
        cmd.addProperty("scoreHome", scoreHome);
        cmd.addProperty("scoreGuest", scoreGuest);
        cmd.addProperty("scored", team.name());
        return cmd;
    }

    public static PushCommand getOpponentUsingItem(GameItem.ItemType type, long duration, boolean enabled) {
        PushCommand cmd = new PushCommand(OPPONENT_ITEM_USE);
        cmd.addProperty("item", type.name());
        cmd.addProperty("duration", duration);
        cmd.addProperty("robot", enabled ? "enabled" : "disabled");
        return cmd;
    }

    public static PushCommand getOpponentItemExpired(GameItem.ItemType type, boolean enabled) {
        PushCommand cmd = new PushCommand(OPPONENT_ITEM_EXPIRED);
        cmd.addProperty("item", type.name());
        cmd.addProperty("robot", enabled ? "enabled" : "disabled");
        return cmd;
    }

    public static PushCommand getOwnItemExpired(GameItem.ItemType type) {
        PushCommand cmd = new PushCommand(OWN_ITEM_EXPIRED);
        cmd.addProperty("item", type.name());
        return cmd;
    }

    public static PushCommand getItemCollected(GameItem.ItemType type, int slot) {
        PushCommand cmd = new PushCommand(ITEM_COLLECTED);
        cmd.addProperty("item", type.name());
        cmd.addProperty("slot", slot);
        return cmd;
    }

    public static PushCommand getGamePaused() {
        return new PushCommand(GAME_PAUSE);
    }


}
