package com.kaercher.roboleague.middleware.controller;

import com.kaercher.roboleague.middleware.factory.PushCommandFactory;
import com.kaercher.roboleague.middleware.model.GameStatus;
import com.kaercher.roboleague.middleware.service.MessageBus;

import java.util.Timer;
import java.util.TimerTask;

class CountdownController {

    private final static long DELAY = 1000L;
    private final static long INTERVAL = 1000L;

    private CountdownController() {
    }

    static void startCountdownAndWatcher(GameWatcher watcher, GameStatus status) {
        Timer timer = new Timer();
        sendCountdown(status);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                int countdown = status.getCountdownTime();
                status.setCountdownTime(--countdown);
                sendCountdown(status);
                if (countdown <= 0) {
                    status.setStatus(GameStatus.Status.PLAYING);
                    sendGameStart(status);
                    watcher.start();
                    cleanup(timer);
                }
            }
        }, DELAY, INTERVAL);
    }

    static void startCountdown(GameStatus status) {
        Timer timer = new Timer();
        sendCountdown(status);
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                int countdown = status.getCountdownTime();
                status.setCountdownTime(--countdown);
                sendCountdown(status);
                if (countdown <= 0) {
                    status.setStatus(GameStatus.Status.PLAYING);
                    sendGameStart(status);
                    cleanup(timer);
                }
            }
        }, DELAY, INTERVAL);
    }

    private static void cleanup(Timer t) {
        t.cancel();
        t.purge();
    }

    private static void sendCountdown(GameStatus gameStatus) {
        MessageBus.getInstance().sendBoth(PushCommandFactory.getCountdown(gameStatus.getCountdownTime()));
    }

    private static void sendGameStart(GameStatus gameStatus) {
        MessageBus.getInstance().sendBoth(PushCommandFactory.getGameStart(gameStatus.getRemainingTime(), false));
    }
}
