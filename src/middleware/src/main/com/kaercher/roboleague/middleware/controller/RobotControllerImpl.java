package com.kaercher.roboleague.middleware.controller;

import com.google.gson.Gson;
import com.kaercher.roboleague.middleware.MiddlewareImpl;
import com.kaercher.roboleague.middleware.interfaces.IRobotConnector;
import com.kaercher.roboleague.middleware.interfaces.IRobotController;
import com.kaercher.roboleague.middleware.model.GameItem;
import com.kaercher.roboleague.middleware.model.GameStatus;
import com.kaercher.roboleague.middleware.model.Robot;
import com.kaercher.roboleague.middleware.model.RobotStatistics;
import com.kaercher.roboleague.robotconnector.controller.RobotConnectorImpl;
import com.kaercher.roboleague.robotconnector.dto.Robot_IdDTO;
import com.kaercher.roboleague.robotconnector.dto.Robot_StatusDTO;
import com.kaercher.roboleague.robotconnector.provider.HttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.kaercher.roboleague.middleware.model.GameItem.ItemType.DIZZY;
import static com.kaercher.roboleague.middleware.model.GameItem.ItemType.FREEZE;
import static com.kaercher.roboleague.middleware.model.GameStatus.Status.PLAYING;

public class RobotControllerImpl implements IRobotController {

    private final static Logger LOG = LoggerFactory.getLogger(RobotConnectorImpl.class);

    private MiddlewareImpl middleware;
    private IRobotConnector connector;

    private final long ROBOT_IP_REQUEST_INTERVAL = 30000;
    private final long REQUEST_START_DELAY = 0;

    private HashMap<String, Robot> robots = new HashMap<>();

    public RobotControllerImpl(MiddlewareImpl middleware) {
        this.middleware = middleware;
        this.connector = new RobotConnectorImpl();
        this.pollForRobotsInNetwork();
    }

    private void pollForRobotsInNetwork() {

        Timer timer = new Timer();
        TimerTask task = new TimerTask() {

            private Robot buildRobot(String ip, String uid, String team) {
                Robot newRobot = new Robot();
                newRobot.setIp(ip);
                newRobot.setUid(uid);
                newRobot.setTeam(Robot.RobotTeam.valueOf(team));
                return newRobot;
            }

            @Override
            public void run() {
                // TODO Loaded/First Run finished/in progress indicator for admin view
                List<String> ips = connector.getRobotIpsInNetwork();
                for (String ip : ips) {
                    try {
                        String json = HttpClient.get(ip, "/get/robot_id");
                        Robot_IdDTO result = new Gson().fromJson(json, Robot_IdDTO.class);
                        if (robots.containsKey(result.getUnique_id())) {
                            if (!robots.get(result.getUnique_id()).getInNetwork()) {
                                LOG.info("Robot with id " + result.getUnique_id() + " reconnected");
                                connector.enableDirectMode(robots.get(result.getUnique_id()));
                            }
                            robots.get(result.getUnique_id()).setIp(ip);
                        } else {
                            Optional<String> team = middleware.getDataProvider().getTeamForRobotUid(result.getUnique_id());
                            if (team.isPresent()) {
                                Robot newRobot = this.buildRobot(ip, result.getUnique_id(), team.get());
                                connector.enableDirectMode(newRobot);
                                robots.put(result.getUnique_id(), newRobot);
                                LOG.info("Robot " + newRobot.getUid() + " added to " + newRobot.getTeam().name() + "-team");
                            } else {
                                LOG.warn("Robot with id " + result.getUnique_id() + " is not in db");
                                continue;
                            }
                        }
                        Robot robot = robots.get(result.getUnique_id());
                        robot.setBattery(connector.getBattery(robot));
                    } catch (Exception ex) {
                        LOG.error(ex.getMessage(), ex);
                    }
                }
                robots.values().forEach(robot -> {
                    if (!ips.contains(robot.getIp())) {
                        try {
                            Robot_StatusDTO status = new Gson().fromJson(connector.getStatus(robot), Robot_StatusDTO.class);
                            robot.setCharging(status.charging);
                            robot.setMode(status.mode);
                        } catch (IOException e) {
                            robot.setInNetwork(false);
                            LOG.warn("Robot with id " + robot.getUid() + " is not connected anymore");
                        }
                    }
                });
            }
        };
        timer.scheduleAtFixedRate(task, REQUEST_START_DELAY, ROBOT_IP_REQUEST_INTERVAL);
    }

    @Override
    public void rotate(String uid, long milliseconds) {
        if (!robots.containsKey(uid))
            return;

        Robot r = robots.get(uid);
        this.connector.rotate(r, milliseconds);
    }

    @Override
    public void moveForward(String uid, int distance, int speed) {
        if (!robots.containsKey(uid))
            return;

        Robot r = robots.get(uid);
        if (this.isAllowedToMove(r)) {
            this.connector.moveForward(r, distance, speed);
        }
    }

    @Override
    public void moveLeft(String uid, int angle, int speed) {
        if (!robots.containsKey(uid))
            return;

        Robot r = robots.get(uid);
        if (this.isAllowedToMove(r)) {
            this.connector.moveLeft(r, angle, speed);
        }
    }

    @Override
    public void moveRight(String uid, int angle, int speed) {
        if (!robots.containsKey(uid))
            return;

        Robot r = robots.get(uid);
        if (this.isAllowedToMove(r)) {
            this.connector.moveRight(r, angle, speed);
        }
    }

    @Override
    public boolean activateItem(String uid, int itemSlot) {
        if (!robots.containsKey(uid))
            return false;

        Optional<Robot> optionalRobot = this.getRobotById(uid);
        return optionalRobot.filter(robot -> this.middleware.getGameController().activateItem(robot, itemSlot)).isPresent();
    }

    @Override
    public List<Robot> getRobots() {
        return new ArrayList<>(this.robots.values());
    }

    @Override
    public Optional<Robot> getRobotById(String uid) {
        if (this.robots.containsKey(uid))
            return Optional.of(this.robots.get(uid));
        else
            return Optional.empty();
    }

    @Override
    public RobotStatistics getRobotStatistics(String uid) {
        Optional<Robot> robot = getRobotById(uid);
        return robot.map(this::getRobotStatistics).orElse(null);
    }

    @Override
    public RobotStatistics getRobotStatistics(Robot robot) {
        return this.connector.getRobotStatistics(robot);
    }

    private boolean isAllowedToMove(Robot robot) {
        List<GameItem> items;
        GameStatus status = this.middleware.getGameController().getGameStatus();
        if (robot.getTeam() == Robot.RobotTeam.HOME) {
            items = this.middleware.getGameController().getGameStatus().getActiveGuestItemsSnapshot();
        } else {
            items = this.middleware.getGameController().getGameStatus().getActiveHomeItemsSnapshot();
        }
        boolean result = true;
        if (items.size() > 0)
            result = !items.stream()
                    .map(item -> (item.getType() != DIZZY && item.getType() != FREEZE))
                    .collect(Collectors.toCollection(ArrayList::new))
                    .contains(false);
        result = status.getStatus() == PLAYING && result;
        return result;
    }
}
