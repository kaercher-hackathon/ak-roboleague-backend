package com.kaercher.roboleague.middleware;

import com.kaercher.roboleague.middleware.controller.GameControllerImpl;
import com.kaercher.roboleague.middleware.controller.RobotControllerImpl;
import com.kaercher.roboleague.middleware.factory.PushCommandFactory;
import com.kaercher.roboleague.middleware.interfaces.*;
import com.kaercher.roboleague.middleware.lightsensor.ApiTriggeredGoalService;
import com.kaercher.roboleague.middleware.model.*;
import com.kaercher.roboleague.middleware.util.RandomString;
import com.kaercher.roboleague.mobilepush.controller.WebSocketController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static com.kaercher.roboleague.middleware.model.GameStatus.Status.PLAYING;


public class MiddlewareImpl implements IMiddleware, IClientConnector {

    private final static Logger LOG = LoggerFactory.getLogger(MiddlewareImpl.class);

    private IRobotController robotController;
    private IGameController gameController;
    private IDataProvider dataProvider;
    private IPushConnector pushConnector;

    private HashMap<String, PushCommandReceiver> receiversWaitingForConfirmation = new HashMap<>();


    public MiddlewareImpl(IDataProvider dataProvider) {
        this.dataProvider = dataProvider;
        this.gameController = new GameControllerImpl(this);
        this.robotController = new RobotControllerImpl(this);
        this.pushConnector = new WebSocketController(this);
        this.pushConnector.start();

    }

    @Override
    public IRobotController getRobotController() {
        return this.robotController;
    }

    @Override
    public IGameController getGameController() {
        return this.gameController;
    }

    @Override
    public IDataProvider getDataProvider() {
        return this.dataProvider;
    }

    @Override
    public void clientConnected(PushCommandReceiver receiver) {
        String sessionKey = new RandomString(8).nextString();
        this.receiversWaitingForConfirmation.put(sessionKey, receiver);
        PushCommand cmd = new PushCommand(PushCommand.CommandType.SOCKET_SESSION);
        cmd.addProperty("key", sessionKey);
        receiver.sendCommand(cmd);
    }

    @Override
    public boolean clientSocketSubmit(String key, String uid) {
        PushCommandReceiver receiver = receiversWaitingForConfirmation.get(key);
        if (receiver == null)
            return false;

        Optional<Robot> optRobot = this.robotController.getRobotById(uid);
        if (optRobot.isPresent()) {
            Robot robot = optRobot.get();
            LOG.info(robot.getUid() + " selected");
            if (robot.getTeam() == Robot.RobotTeam.HOME) {
                this.gameController.setHomePushReceiver(receiver);
                this.gameController.setHomeRobot(robot);
                if (this.gameController.getGameStatus().getStatus() == PLAYING)
                    receiver.sendCommand(PushCommandFactory.getGameStart(this.gameController.getGameStatus().getRemainingTime(), false));
                LOG.info("Home connected");
            } else if (robot.getTeam() == Robot.RobotTeam.GUEST) {
                this.gameController.setGuestPushReceiver(receiver);
                this.gameController.setGuestRobot(robot);
                if (this.gameController.getGameStatus().getStatus() == PLAYING)
                    receiver.sendCommand(PushCommandFactory.getGameStart(this.gameController.getGameStatus().getRemainingTime(), false));
                LOG.info("Guest connected");
            }
            robot.setGameStartStatistics(this.robotController.getRobotStatistics(robot));
            this.receiversWaitingForConfirmation.remove(key);
            return true;
        } else {
            receiver.sendCommand(PushCommandFactory.getDenied("robot_not_found"));
            this.pushConnector.disconnectClient(receiver);
            return false;
        }
    }

    @Override
    public RankingPlayer[] getRanking() {
        return this.dataProvider.getTopTenPlayers();
    }

    @Override
    public Statistics getStatistics() {
        Statistics statistics = new Statistics();

        List<RobotStatistics> stats = this.dataProvider.getRobotStatisticsForToday();
        stats.forEach(stat -> statistics.cleanedArea += stat.getDistanceDriven() * 20 / 10000);

        statistics.timeOfCleaning = (this.gameController.getGameStatus().GAME_TIME - this.gameController.getGameStatus().getRemainingTime())
                + this.dataProvider.getMatchCountForToday() * this.gameController.getGameStatus().GAME_TIME;

        statistics.mostGoals = this.dataProvider.getMostGoalsPerMatchForToday();

        statistics.fastestGoal = this.dataProvider.getFastestGoalForToday();

        return statistics;
    }

    @Override
    public void addGuestGoal() {
        ApiTriggeredGoalService.getInstance().setGuestGoal();
    }

    @Override
    public void addHomeGoal() {
        ApiTriggeredGoalService.getInstance().setHomeGoal();
    }

    public void resetSockets() {
        if (this.pushConnector != null)
            this.pushConnector.disconnectClients();
        this.receiversWaitingForConfirmation.clear();
    }
}
