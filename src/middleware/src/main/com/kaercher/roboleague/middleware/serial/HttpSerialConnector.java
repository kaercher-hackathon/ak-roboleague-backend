package com.kaercher.roboleague.middleware.serial;


import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.kaercher.roboleague.middleware.interfaces.ISerialConnector;
import com.kaercher.roboleague.middleware.interfaces.ISerialListener;
import com.kaercher.roboleague.middleware.serial.http.HttpClient;
import com.kaercher.roboleague.middleware.serial.http.SerialConnection;
import com.kaercher.roboleague.middleware.serial.http.SerialEventEmitter;
import com.kaercher.roboleague.middleware.service.EventBus;

import java.io.IOException;
import java.util.HashMap;
import java.util.Optional;
import java.util.Timer;
import java.util.TimerTask;

import static com.kaercher.roboleague.middleware.serial.http.SerialConnection.Light.OFF;
import static com.kaercher.roboleague.middleware.serial.http.SerialConnection.Light.ON;

public class HttpSerialConnector implements ISerialConnector {


    private HashMap<Long, ISerialListener> listener = new HashMap<>();
    private final String URL = "http://192.168.4.1:8134";//"http://127.0.0.1:8134";

    private Timer serialQueryTimer;

    private HashMap<String, SerialConnection> ttyConnections = new HashMap<>();

    @Override
    public ISerialListener addRowListener(long time, ISerialListener listener) {
        this.listener.put(time, listener);
        return listener;
    }

    @Override
    public void removeRowListener(long time) {
        this.listener.remove(time);
    }

    @Override
    public void connect() throws IOException, InterruptedException {
        TimerTask task = new TimerTask() {
            @Override
            public void run() {

                try {
                    String json = HttpClient.get(URL + "/list");
                    JsonArray result = new Gson().fromJson(json, JsonArray.class);
                    removeOldConnections(result);
                    addNewConnectionForTTYResults(result);
                } catch (IOException e) {
                    //e.printStackTrace();
                }
            }
        };
        if (this.serialQueryTimer != null) {
            this.serialQueryTimer.cancel();
            this.serialQueryTimer.purge();
        }
        this.serialQueryTimer = new Timer();
        this.serialQueryTimer.scheduleAtFixedRate(task, 0, 15000);
    }

    private void removeOldConnections(JsonArray ttyResult) {
        ttyConnections.keySet().forEach(key -> {
            final boolean[] exists = {false};
            ttyResult.forEach(jsonElement -> {
                exists[0] = jsonElement.getAsString().equals(key) || exists[0];
            });
            if (!exists[0]) {
                ttyConnections.get(key).end();
                ttyConnections.remove(key);
            }
        });
    }

    private void addNewConnectionForTTYResults(JsonArray ttyResult) {
        ttyResult.forEach(item -> {
            String interfaceName = item.getAsString();
            if (ttyConnections.containsKey(interfaceName))
                return;


            SerialConnection connection = new SerialConnection(URL, interfaceName, buildSerialEventEmitter());
            ttyConnections.put(interfaceName, connection);
            EventBus.getInstance().emmit("tty_connect", interfaceName);
            connection.start();
        });
    }

    private SerialEventEmitter buildSerialEventEmitter() {
        return (tty, arduinoId, message) -> listener.values().forEach(l -> l.messageArrived(tty, arduinoId, message));
    }

    @Override
    public void disconnect() {
        this.ttyConnections.values().forEach(SerialConnection::end);
        this.serialQueryTimer.cancel();
        this.serialQueryTimer.purge();
    }

    @Override
    public void sendMessage(int arduinoID, String message) {
        Optional<SerialConnection> optional = this.ttyConnections.values().stream().filter(con -> con.getArduinoID() == arduinoID).findFirst();
        if (!optional.isPresent())
            return;
        if (message.equals("LIGHT_ON"))
            optional.get().setLight(ON);
        if (message.equals("LIGHT_OFF"))
            optional.get().setLight(OFF);
    }
}
