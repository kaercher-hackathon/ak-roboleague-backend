package com.kaercher.roboleague.middleware.lightsensor;

import com.kaercher.roboleague.middleware.interfaces.ILightSensor;
import com.kaercher.roboleague.middleware.model.Robot.RobotTeam;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.GUEST;
import static com.kaercher.roboleague.middleware.model.Robot.RobotTeam.HOME;

public class ApiTriggeredLightSensorImpl implements ILightSensor {

    private static final Logger LOG = LoggerFactory.getLogger(ApiTriggeredLightSensorImpl.class);
    private boolean homeGoal = false;
    private boolean guestGoal = false;

    public ApiTriggeredLightSensorImpl() {
        ApiTriggeredGoalService.getInstance().setLightSensorImpl(this);
    }

    @Override
    public boolean objectPresent(RobotTeam team) {
        if (team == HOME) {
            if (homeGoal) {
                this.homeGoal = false;
                return true;
            }
            return false;
        }
        if (team == GUEST) {
            if (guestGoal) {
                this.guestGoal = false;
                return true;
            }
            return false;
        }
        return false;
    }

    public void setHomeGoal() {
        this.homeGoal = true;
    }

    public void setGuestGoal() {
        this.guestGoal = true;
    }
}
