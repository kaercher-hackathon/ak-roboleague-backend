package com.kaercher.roboleague;

import com.kaercher.roboleague.api.Api;
import com.kaercher.roboleague.dataprovider.connectors.NoStorageConnector;
import com.kaercher.roboleague.dataprovider.connectors.SQLiteProvider;
import com.kaercher.roboleague.middleware.MiddlewareImpl;
import com.kaercher.roboleague.middleware.interfaces.IMiddleware;

import java.io.File;

public class Program {

    public static void main(String[] args) {
        System.out.println("Program startup...");

        /*System.out.println("Check if db present");
        if (new File(SQLiteProvider.DB_PATH).exists())
            SQLiteProvider.getInstance().connect();
        else {
            SQLiteProvider.getInstance().createDB();
            SQLiteProvider.getInstance().connect(); //optional
        }
        System.out.println("DB loaded");*/

        System.out.println("Starting Middleware");
        IMiddleware middleware = new MiddlewareImpl(new NoStorageConnector());
        System.out.println("Middleware started");

        System.out.println("Starting Api");
        Api api = new Api();
        api.start(middleware);
        System.out.println("Api started");

    }
}
