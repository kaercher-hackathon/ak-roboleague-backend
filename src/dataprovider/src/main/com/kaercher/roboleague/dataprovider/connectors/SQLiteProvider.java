package com.kaercher.roboleague.dataprovider.connectors;

import com.kaercher.roboleague.middleware.dbo.RobotTeamDbo;
import com.kaercher.roboleague.middleware.interfaces.IDataProvider;
import com.kaercher.roboleague.middleware.model.GameStatus;
import com.kaercher.roboleague.middleware.model.RankingPlayer;
import com.kaercher.roboleague.middleware.model.Robot;
import com.kaercher.roboleague.middleware.model.RobotStatistics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Optional;


public class SQLiteProvider implements IDataProvider {

    private static Logger LOG = LoggerFactory.getLogger(SQLiteProvider.class);
    public static final String DB_PATH = "./db/data.db";
    private static final SQLiteProvider INSTANCE = new SQLiteProvider();

    private Connection connection;

    static {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            LOG.error("failed loading jdbc driver", e);
        }
    }

    private SQLiteProvider() {
    }

    public static SQLiteProvider getInstance() {
        return INSTANCE;
    }

    private void initDBConnection() {
        try {
            LOG.info("Try to open db: " + DB_PATH);
            if (connection != null || !new File(DB_PATH).exists())
                return;

            LOG.info("Creating Connection to Database...");
            connection = DriverManager.getConnection("jdbc:sqlite:" + DB_PATH);
            if (!connection.isClosed())
                LOG.info("...Connection established");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            try {
                if (!connection.isClosed() && connection != null) {
                    connection.close();
                    if (connection.isClosed())
                        LOG.info("Connection to Database closed");
                }
            } catch (SQLException e) {
                LOG.error(e.getMessage(), e);
            }
        }));
    }

    public void connect() {
        this.initDBConnection();
    }

    public void createDB() {
        String stmt = "CREATE TABLE gameresult(id INTEGER PRIMARY KEY AUTOINCREMENT, homeName TEXT, homeGoals INTEGER, homePoints INTEGER, guestName TEXT, guestGoals INTEGER, \n" +
                "guestPoints INTEGER [currentime] TIMESTAMP  NULL);\n" +
                "\n" +
                "CREATE TABLE [player_results] (\n" +
                "[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "[name] TEXT  NOT NULL,\n" +
                "[points] INTEGER  NOT NULL,\n" +
                "[created_date] TIMESTAMP  NOT NULL\n" +
                ");" +
                "CREATE TABLE [robot_team] (\n" +
                "[id] INTEGER  NOT NULL PRIMARY KEY AUTOINCREMENT,\n" +
                "[uid] TEXT  UNIQUE NOT NULL,\n" +
                "[team] TEXT  NULL\n" +
                ");\n";
        try {
            File dir = new File(DB_PATH).getParentFile();
            if (!dir.exists())
                if (dir.mkdir() && new File(DB_PATH).createNewFile())
                    LOG.info("Database created.");
        } catch (IOException e) {
            LOG.error(e.getMessage(), e);
        }
        this.initDBConnection();
        try {
            Statement statement = connection.createStatement();
            statement.executeUpdate(stmt);
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }

    }


    @Override
    public Optional<String> getTeamForRobotUid(String uid) {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT [team] FROM [robot_team] WHERE [uid] = ?;");
            ps.setString(1, uid);
            ResultSet rs = ps.executeQuery();
            if (!rs.next())
                return Optional.empty();
            String team = rs.getString("team");
            rs.close();
            return Optional.of(team);
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    @Override
    public Optional<String> getTeamForTagName(String tag) {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT [team] FROM [robot_team] WHERE [tag] = ?;");
            ps.setString(1, tag);
            ResultSet rs = ps.executeQuery();
            if (!rs.next())
                return Optional.empty();
            String team = rs.getString("team");
            rs.close();
            return Optional.of(team);
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    @Override
    public Optional<RobotTeamDbo> getRobotTeamByUid(String uid) {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT [id], [team], [tag] FROM [robot_team] WHERE [uid] = ?;");
            ps.setString(1, uid);
            ResultSet rs = ps.executeQuery();
            if (!rs.next())
                return Optional.empty();
            RobotTeamDbo robot = new RobotTeamDbo();
            robot.uid = uid;
            robot.id = rs.getInt("id");
            robot.tag = rs.getString("tag");
            robot.team = rs.getString("team");
            rs.close();
            return Optional.of(robot);
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    @Override
    public Optional<RobotTeamDbo> getRobotTeamByTag(String tag) {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT [id], [team], [uid] FROM [robot_team] WHERE [tag] = ?;");
            ps.setString(1, tag);
            ResultSet rs = ps.executeQuery();
            if (!rs.next())
                return Optional.empty();
            RobotTeamDbo robot = new RobotTeamDbo();
            robot.tag = tag;
            robot.uid = rs.getString("uid");
            robot.id = rs.getInt("id");
            robot.team = rs.getString("team");
            rs.close();
            return Optional.of(robot);
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return Optional.empty();
    }

    @Override
    public RankingPlayer[] getTopTenPlayers() {
        RankingPlayer[] players = new RankingPlayer[10];
        for (int i = 0; i < 10; i++) {
            players[i] = null;
        }

        try {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT [name],[points],[created_at] FROM [player_results] WHERE [created_at] >= ? ORDER BY [points] DESC LIMIT 0,9;");


            Calendar queryTime = Calendar.getInstance(); //this instance gets the current date/time as default values
            queryTime.set(Calendar.HOUR, 0);    //setting time to 0, as its set to current time by default.
            queryTime.set(Calendar.MINUTE, 0);

            ps.setLong(1, queryTime.getTimeInMillis());

            ResultSet rs = ps.executeQuery();

            int counter = 0;

            while (rs.next()) {
                RankingPlayer player = new RankingPlayer(counter + 1, rs.getInt("points"), rs.getString("name"));
                players[counter] = player;
                counter++;
            }
            rs.close();
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return players;
    }

    @Override
    public void saveGameResult(GameStatus status) {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO [gameresult] ([homeName], [homeGoals], [homePoints], [guestName], [guestGoals], [guestPoints])" +
                            "VALUES (?,?,?,?,?,?);");
            ps.setString(1, status.getHomeName());
            ps.setInt(2, status.getHomeGoals());
            ps.setInt(3, status.getHomePoints());
            ps.setString(4, status.getGuestName());
            ps.setInt(5, status.getGuestGoals());
            ps.setInt(6, status.getGuestPoints());
            ps.execute();

            ps.close();
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void savePlayerResult(String name, int goals, int points) {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO [player_results] ([name], [goals], [points])" +
                            "VALUES (?,?,?);");
            ps.setString(1, name);
            ps.setInt(2, goals);
            ps.setInt(3, points);
            ps.execute();

            ps.close();
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public void saveRobotStatistics(Robot robot) {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("INSERT INTO [robot_statistics] ([uid], [distance], [time], [area])" +
                            "VALUES (?,?,?,?);");
            ps.setString(1, robot.getUid());
            ps.setInt(2, robot.getGameEndStatistics().getDistanceDriven() - robot.getGameStartStatistics().getDistanceDriven());
            ps.setInt(3, robot.getGameEndStatistics().getCleaningTime() - robot.getGameStartStatistics().getCleaningTime());
            ps.setInt(4, robot.getGameEndStatistics().getAreaCleaned() - robot.getGameStartStatistics().getAreaCleaned());
            ps.execute();

            ps.close();
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
    }

    @Override
    public List<RobotStatistics> getRobotStatisticsForToday() {
        List<RobotStatistics> list = new ArrayList<>();
        try {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT [distance],[time], [area], [created_at] FROM [robot_statistics] WHERE [created_at] >= ?;");


            Calendar queryTime = Calendar.getInstance(); //this instance gets the current date/time as default values
            queryTime.set(Calendar.HOUR, 0);    //setting time to 0, as its set to current time by default.
            queryTime.set(Calendar.MINUTE, 0);

            ps.setLong(1, queryTime.getTimeInMillis());

            ResultSet rs = ps.executeQuery();


            while (rs.next()) {
                RobotStatistics statistics = new RobotStatistics(rs.getInt("distance"), rs.getInt("time"), rs.getInt("area"));
                list.add(statistics);
            }
            rs.close();
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return list;
    }

    @Override
    public int getMatchCountForToday() {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT COUNT([id]) FROM [robot_statistics] WHERE [created_at] >= ?;");

            Calendar queryTime = Calendar.getInstance();
            queryTime.set(Calendar.HOUR, 0);
            queryTime.set(Calendar.MINUTE, 0);

            ps.setLong(1, queryTime.getTimeInMillis());

            ResultSet rs = ps.executeQuery();

            rs.next();
            int count = rs.getInt(1);
            rs.close();
            return count;
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return 0;
    }

    @Override
    public int getMostGoalsPerMatchForToday() {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT SumGoals " +
                            "FROM (SELECT (homeGoals + guestGoals) AS SumGoals FROM gameresult WHERE [created_at] >= ?) tmp " +
                            "ORDER BY SumGoals DESC LIMIT 0,1;");

            Calendar queryTime = Calendar.getInstance();
            queryTime.set(Calendar.HOUR, 0);
            queryTime.set(Calendar.MINUTE, 0);

            ps.setLong(1, queryTime.getTimeInMillis());

            ResultSet rs = ps.executeQuery();

            if (!rs.next())
                return 0;
            int count = rs.getInt(1);
            rs.close();
            return count;
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return 0;
    }

    @Override
    public int addPlayer(String name) {
        int playerID = 0;
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO [player] ([name]) VALUES(?)",
                    Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, name);

            int affectedRows = statement.executeUpdate();
            if (affectedRows == 0)
                throw new SQLException("Creating user failed, no rows affected.");

            try (ResultSet generatedKeys = statement.getGeneratedKeys()) {
                if (generatedKeys.next())
                    playerID = generatedKeys.getInt(1);
                else
                    throw new SQLException("Creating user failed, no ID obtained.");
            }
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return playerID;
    }

    @Override
    public long getFastestGoalForToday() {
        try {
            PreparedStatement ps = connection
                    .prepareStatement("SELECT [passed_time] FROM [goals] WHERE [created_at] >= ? " +
                            "ORDER BY [passed_time] DESC LIMIT 0,1;");

            Calendar queryTime = Calendar.getInstance();
            queryTime.set(Calendar.HOUR, 0);
            queryTime.set(Calendar.MINUTE, 0);

            ps.setLong(1, queryTime.getTimeInMillis());

            ResultSet rs = ps.executeQuery();

            if (!rs.next())
                return 0;
            long time = rs.getLong(1);
            rs.close();
            return time;
        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
        return 0;
    }

    @Override
    public void saveNewGoal(int playerID, long time) {
        try {
            PreparedStatement statement = connection.prepareStatement("INSERT INTO [goals] ([player], [passed_time]) VALUES(?,?)");
            statement.setInt(1, playerID);
            statement.setLong(2, time);

            statement.executeUpdate();

        } catch (SQLException e) {
            LOG.error(e.getMessage(), e);
        }
    }
}
