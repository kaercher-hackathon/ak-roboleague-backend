package com.kaercher.roboleague.dataprovider.connectors;

import com.kaercher.roboleague.middleware.dbo.RobotTeamDbo;
import com.kaercher.roboleague.middleware.interfaces.IDataProvider;
import com.kaercher.roboleague.middleware.model.GameStatus;
import com.kaercher.roboleague.middleware.model.RankingPlayer;
import com.kaercher.roboleague.middleware.model.Robot;
import com.kaercher.roboleague.middleware.model.RobotStatistics;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class NoStorageConnector implements IDataProvider {
    @Override
    public Optional<String> getTeamForRobotUid(String uid) {
        if (uid.equals("AAAFqBagWoCqdLBrS4QD6Q"))
            return Optional.of("HOME");
        if (uid.equals("AAA7cuzIsSN6eJRrQxBiwA"))
            return Optional.of("GUEST");
        return Optional.empty();
    }

    @Override
    public Optional<String> getTeamForTagName(String tag) {
        if (tag.equals("robot1"))
            return Optional.of("HOME");
        if (tag.equals("robot2"))
            return Optional.of("GUEST");
        return Optional.empty();
    }

    @Override
    public Optional<RobotTeamDbo> getRobotTeamByUid(String uid) {
        RobotTeamDbo obj = new RobotTeamDbo();
        obj.id = 0;
        obj.uid = uid;
        if (uid.equals("AAAFqBagWoCqdLBrS4QD6Q")) {
            obj.team = "HOME";
            obj.tag = "robo1";
        }
        if (uid.equals("AAA7cuzIsSN6eJRrQxBiwA")) {
            obj.team = "GUEST";
            obj.tag = "robo2";
        }
        return Optional.of(obj);
    }

    @Override
    public Optional<RobotTeamDbo> getRobotTeamByTag(String tag) {
        RobotTeamDbo obj = new RobotTeamDbo();
        obj.id = 0;
        obj.tag = tag;
        if (tag.equals("robo1")) {
            obj.team = "HOME";
            obj.uid = "AAAFqBagWoCqdLBrS4QD6Q";
        }
        if (tag.equals("robo2")) {
            obj.team = "GUEST";
            obj.uid = "AAA7cuzIsSN6eJRrQxBiwA";
        }
        return Optional.of(obj);
    }

    @Override
    public RankingPlayer[] getTopTenPlayers() {
        return new RankingPlayer[10];
    }

    @Override
    public void saveGameResult(GameStatus status) {

    }

    @Override
    public void savePlayerResult(String name, int goals, int points) {

    }

    @Override
    public void saveRobotStatistics(Robot robot) {

    }

    @Override
    public List<RobotStatistics> getRobotStatisticsForToday() {
        return new ArrayList<>();
    }

    @Override
    public int getMatchCountForToday() {
        return 0;
    }

    @Override
    public int getMostGoalsPerMatchForToday() {
        return 0;
    }

    @Override
    public int addPlayer(String name) {
        return 0;
    }

    @Override
    public long getFastestGoalForToday() {
        return 0;
    }

    @Override
    public void saveNewGoal(int playerID, long time) {

    }
}
