# RoboLeague Backend

## What is RoboLeague Backend?

The RoboLeague backend is a system for controlling the game components within the framework of the RoboLeague rules. The different components are:

- RFID reader
- Cleaning robot RC3
- Mobile App

The aim of the game is to achieve a high score by triggering various events. In detail see [Rules -> Points](docs/rules.md)

The system provides a number of different [APIs](docs/apis.md) through which the status of the game can be retrieved and manipulated.

## Component Architecture

![Component architecture](docs/component_architecture.jpg "componentd architecture")

## Folder structure

- **/db** - clear db included
- **/dist** - distribution folder of the software (ready-to-run)
- **/docs** - documentation
- **/lib** - 3rd party lib folder
- **/src** - sources for backend, arduino and serial api

The structure of the dist folder for the backend software looks like this:

- **/db**
    - data.db
- **/libs**
    - *.jar
- **Program.jar**
- **start.bat** - Windows start script
- **start** - Unix start script

## Installation

#### Dependencies:

- pip
- nmap
- SQLite 3

#### Get pip
```bash
sudo curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo python3.5 get-pip.py
```

#### Get nmap & SQLite
```bash
sudo apt-get update
sudo apt-get upgrade
sudo apt-get install nmap sqlite3
```

#### Install pyserial with dependencies

Navigate to pyserial folder (/src/pyserial-api) and run the command:
```bash
python3.5 -m pip install -r requirements.txt
```
After dependencies are installed copy or link the pyserial-api.init-script to /etc/init.d/ and add it as autostart service
```bash
sudo cp pyserial-api.init /etc/init.d/pyserial-api
sudo update-rc.d pyserial-api defaults
```
Now you can run `sudo /etc/init.d/pyserial-api [start|stop|restart|status]`

#### Install program startup script

Navigate to the source folder (/src) and run the following commands
```bash
sudo cp rl-backend.init /etc/init.d/rl-backend
sudo update-rc.d rl-backend defaults
```

Now you can run `sudo /etc/init.d/rl-backend [start|stop|restart|status]`

## Manual start-up

The software can also be started manually. To do this, follow the steps below.


Run `java -jar ./Program.jar` from the command line. You **must** run the program from the dist folder as execution path.

### Pyserial API

In addition to the backend software, the pyserial api must be run on the raspberry pi to enable communication with the RFID readers.

#### Get pip
```bash
sudo curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
sudo python3.5 get-pip.py
```
#### Install pyserial dependencies

Navigate to pyserial folder (/src/pyserial-api) and run the command:
```bash
python3.5 -m pip install -r requirements.txt
```
After dependencies are installed run `sudo python3.5 main.py` to start the pyserial api. 

## Component explanation

### RoboLeague Backend (+ Pyserial-Api)

The RoboLeague backend is the "brain" of the game. It controls whether robots can be moved, reads the messages from the communication with the RFID readers and manages the game status.

### RFID Reader

The RFID readers (model PN532) are read out via an Arduino Nano. These are supplied via a mini-USB connection with power over which the communication via serial interface is made possible. The exchange of messages is carried out via a custom message format.
In addition, the LEDs connected to the Arduino can be switched on and off via the interface.

### Mobile App

The mobile app is not part of the backend and has its own project. The app allows the player to register for his robot and take control. The app can also be used to activate collected items as well as view the current game status. Information is provided to the app via websockets.

### Scoreboard/Gamestatus

The scoreboard can display the current game status. Part of the status are, among other things, the names, scores and goals of the teams, the activated item, the remaining playing time and the display of a countdown.

### RoboCleaner RC3

The RoboCleaner RC3 is an autonomous vacuum robot from Kärcher. He can independently clean after a manual start or on a schedule. As part of the RoboLeague, it also provides an interface for direct control over which the robot can navigate forward, left and right.