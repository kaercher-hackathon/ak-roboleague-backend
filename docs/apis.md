# RoboLeague APIS

The RoboLeague backend has several APIs. The APIs are classified as follows:

- REST API
- Socket API
- Serial API
- Robot direct control API

## REST API

The REST API definition can be found in the [Swagger File](api-definition.yaml).

## Socket API

The socket API is implemented with websockets. The message structure can also be adopted for other sockets.

The message structure is as follows:
```
MESSAGE_TYPE?param1=value1&param2=value2
```
### Message types

#### GAME_START

Is sent when game is started/resumed AND after countdown finished.

Params:
- time // <- remaining play time in ms
- countdown // <-- if countdown will start [true/false]

#### GAME_END

Is sent when the play time is set to 0

Params:
- scoreHome // <-- number of home goals [integer]
- scoreGuest // <-- number of guest goals [integer]
- pointsHome // <-- number of home points [integer]
- pointsGuest // <-- number of guest points [integer]
- winner // <-- winner of the game [home/guest]


#### GAME_PAUSE

Is sent when the game state is set to BREAK

#### TIME

Is sent every 100ms when game state is PLAYING

Params: 
- remaining // <-- remaining play time in ms

#### COUNTDOWN

Is sent every second after game started or resumed until countdown value is 0.

Params:
- value // <-- countdown value [integer]

#### SCORE_CHANGED

Is sent after a team scored a goal. All active items will be removed.

Params:
- scoreHome // <-- number of home goals after score change [integer]
- scoreGuest // <-- number of guest goals after score change [integer]
- scored // <-- team that scored [home/guest]

#### OPPONENT_ITEM_USE

Is sent after the opponent used an item. Useful to update the GUI based on the used item.

Params:
- item // <-- type of the used item [DIZZY/FREEZE/DOUBLE_GOAL]
- duration // <-- duration of the item in ms
- robot // <-- if the robot is enabled (FREEZE & DIZZY disables robot) [enabled/disabled]

#### OPPONENT_ITEM_EXPIRED

Is sent after the opponents item duration is set to 0.
Not sent after SCORE_CHANGED.

Params:
- item // <-- type of the used item [DIZZY/FREEZE/DOUBLE_GOAL]
- robot <-- if the robot is enabled (should be true) [enabled/disabled]

#### OWN_ITEM_EXPIRED

Is sent after the own active item duration is set to 0.
Not sent after SCORE_CHANGED.

Params:
- item // <-- type of the used item [DIZZY/FREEZE/DOUBLE_GOAL]

#### ITEM_COLLECTED

Is sent after the own robot collected an item

Params:
- item // <-- type of the used item [DIZZY/FREEZE/DOUBLE_GOAL]

#### SOCKET_SESSION

Is sent immediately after the connection is established.

Params:
- key // <-- session key which is used to register the socket via REST API

#### DENIED

Is sent if the registration of the socket was denied from the server

Params:
- reason // <-- reason of the forced closing

## Serial API

The Serial API is provided by the pyserial-api service and is seen exactly a REST interface.

Sie hat eine List Route und 2 generische Routen die aufgerufen werden können.

#### GET /list

Returns an array of connected `ttyUSB` devices
 ```json
 [
     "ttyUSB0",
     "ttyUSB1",
     "ttyUSB2"
 ]
 ```

 #### GET /ttyUSB[x]/out

 This route is a longpolling route. It waits until an result is sent. The received message is removed from the message stack of the pyserial-api.

 The result is a message with the following format:

 ```
 arduino:[MESSAGE_TYPE]:[ARDUINO_ID]:[NFC_CARD_CONTENT]
 ```

 The following message types can be returned:
 - nfc
 - init

**Sample messages**

```
arduino:init:2          // sent once after the pyserial-api connects to the serial interface of the Arduino
arduino:nfc:1:enHello   // sent after reading an RFID tag
```

> Warning: There will be 2 other messages returned which will not fit in the message format. These messages are from the library which is used to read the RFID tags and will be sent after the pyserial-api connects to the serial interface of the Arduino.

#### PUT /ttyUSB[x]/in

This route is to activate or deactivate the LEDs connected to the Arduino.

The body should be a `0` or a `1` followed by a new line (`\n`)

**Pseudo code**
```javascript
const body = '1\n';
request(`${ip}/ttyUSB1/in`, body, {method: 'PUT'});
```

## Robot direct control API

The Robot direct control API definition can be found in the [Swagger File](robot-directcontrol.yaml).