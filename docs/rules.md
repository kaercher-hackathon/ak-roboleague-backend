# RoboLeague Rules


## Course of the game:
- Playtime: 6min
- The winner is the player with the most points (Look at [Points](#Points) to see how you can get points).
- RC3's are standing in front of their goal, the ball is placed in the middle
- A maximum of 2 specials can be collected simultaneously. 
- Only one special can be activated at time.
- After a used special is expired there is a cooldown of 5 seconds until the 2nd special can be activated.
- The playing field may not be entered.

## Lineup
- Start of the RC3 at your own gate
- In the case of a goal, the rules apply analogously to football; RC3 of the goalscorer at the goal, opponent has kick-off
- When the ball is jammed, the ball is placed in the middle again.
- Players always stand behind their goal
- If the RC3s getting blocked, they will be separated manually and brought back to the goals.

## Points
Points are awarded for the following activities
- Goals (+50pts)
- Extra points for Hattrick (3 goals in one game) (+50pts)
- Collection items (Use of NFC tags) (+10pts)
- Using items (+10pts)
- Collection "star"-item (+50pts)

## PowerUps/Specials
- 1x Double: The next goal counts double (PowerUp is valid for 60 seconds after activation)
    - Placement in the middle of the field (left)
- 2x Freeze: The opponent is frozen for 5 seconds.
    - Placement on each half of the game, mirrored
- 2x Dizzy: Opponent spins in a circle for 3 seconds
    - Placement on each half of the game, mirrored
- 1x Dong: Player get 50pts immediately 
    - Placement in the middle of the field (right)